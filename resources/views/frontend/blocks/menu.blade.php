<header class="header">
    <div class="container-menu-header">
        <div class="topbar">
            <div class="block-content">
                <div class="block-html-content " style="  ">
                <p><span style="color: #ff0000;">
                    <strong>HOTLINE&nbsp;</strong></span>&nbsp;
                    {!! $hotline1->content !!} <span style="color: #3366ff;"><strong>{!! $phonename1->phonename !!}</strong></span>&nbsp;<!-- -&nbsp; -->
                    {!! $hotline2->content !!}<span style="color: #3366ff;"><strong> {!! $phonename2->phonename !!}</strong></span>&nbsp;<!-- -&nbsp; -->
                    {!! $hotline3->content !!}<span style="color: #3366ff;"><strong>{!! $phonename3->phonename !!}</strong></span>
                </p>        
                </div>
            </div>
          
            <div class="user">

                <ul class="nav navbar-nav pull-right" style="flex-direction: initial;">
                    @if(!isset($user))
                    <li class="user-li">
                        <a href="{{route('dangky')}}"><i class="fas fa-star"></i>Đăng ký</a>
                    </li>
                    <li class="user-li">
                        <a href="{{route('dangnhap')}}"><i class="fas fa-sign-in-alt"></i>Đăng nhập</a>
                    </li>
                    @else
                    <li class="user-li">
                        <a><i class="fas fa-user"></i>Chào: {{$user->username}}</a>
                    </li>

                    <li class="user-li">
                        <a href="{{route('quanlytk.update')}}"><i class="fas fa-info-circle"></i>Quản lý tài khoản</a>
                    <li class="user-li">
                        <a href="{{route('dangxuat')}}"><i class="fas fa-sign-out-alt"></i>Đăng xuất</a>
                    </li>
                    @endif
                </ul>
            </div>
        </div>

        <div class="wrap-header">
            <a href="{{route('home')}}" class="logo">
                <img src="{{asset('frontend/img/logo.jpg')}}">
            </a>
            <div class="wrap-menu">
                <nav class="menu">
                    <ul class="main-menu">
                        <li><a href="{{route('home')}}">Trang chủ</a></li>
                        <li><a href="{{route('gioi-thieu')}}">Giới thiệu</a></li>
                        <?php
                        $menu_lv_1 = DB::table('bds_dmtd')->where('parent_id', 0)->get();
                        ?>
                        @foreach($menu_lv_1 as $item_lv_1)
                        <li>
                            <a href="{{url('product-category-parent',[$item_lv_1->alias])}}">{{$item_lv_1->name}}</a>
                            <ul class="sub-menu">
                                <?php
                                $menu_lv_2 = DB::table('bds_dmtd')->where('parent_id', $item_lv_1->id)->get();
                                ?>
                                @foreach($menu_lv_2 as $item_lv_2)
                                <li><a href="{{url('product-category',[$item_lv_2->alias,$item_lv_2->parent_id])}}">{{ $item_lv_2->name }}</a></li>
                                @endforeach
                            </ul>
                        </li>
                        @endforeach
                        <li><a href="{{route('project')}}">Dự án</a></li>

                        @foreach($menu as $item)
                            <li>
                                <a href="{{ $item->link }}">{{ $item->name }}</a>
                            </li>
                        @endforeach
                        <li class="page_menu_item menu_mm"><a href="{{route('contact-company')}}">Liên Hệ</a></li>

                    </ul>
                </nav>
            </div>
        </div>

        <div class="wrap-header-mobile">
            <a href="{{route('home')}}" class="logo">
                <img src="{{asset('frontend/img/logo.jpg')}}">
            </a>
            <div class="show-menu">
                <i class="fas fa-user mr-3 user-icon" style="font-size:20px"></i>
                <div class="bar-menu">
                    <i class="fa fa-bars "></i>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Menu -->
<div class="menux menu_mm trans_300">
    <div class="menu_container menu_mm">
        <div class="page_menu_content">
            <ul class="page_menu_nav menu_mm">
                <li class="page_menu_item has-children menu_mm">
                    <a href="{{route('home')}}">Trang chủ</a>
                </li>
                <li class="page_menu_item menu_mm"><a href="{{route('gioi-thieu')}}">Giới thiệu</a></li>

                <?php
                $menu_lv_1 = DB::table('bds_dmtd')->where('parent_id', 0)->get();
                ?>
                @foreach($menu_lv_1 as $item_lv_1)
                <li class="page_menu_item has-children menu_mm">
                    <a href="{{url('product-category',[$item_lv_1->alias])}}">{{ $item_lv_1->name}}</a>
                    <ul class="page_menu_selection menu_mm">
                        <?php
                        $menu_lv_2 = DB::table('bds_dmtd')->where('parent_id', $item_lv_1->id)->get();
                        ?>
                        @foreach($menu_lv_2 as $item_lv_2)
                        <li class="page_menu_item menu_mm">
                            <a href="{{url('product-category',[$item_lv_2->alias,$item_lv_2->parent_id])}}">{{ $item_lv_2->name }}</a></li>
                        @endforeach
                    </ul>
                </li>
                @endforeach
                <li class="page_menu_item menu_mm"><a href="{{route('project')}}">Dự án</a></li>
                <li class="page_menu_item menu_mm"><a href="{{route('news')}}">Tin tức</a></li>
                <li class="page_menu_item menu_mm"><a href="{{route('tuyen-dung')}}">Tuyển dụng</a></li>

            </ul>
        </div>
    </div>
</div>


<!-- Quan ly tai khoan -->
<div class="menuy menu_mm trans_300">
    <div class="menu_container menu_mm">
        <div class="page_menu_content">
            <ul class="page_menu_nav menu_mm">
                @if(!isset($user))
                    <li class="page_menu_item menu_mm"><a href="{{route('dangnhap')}}">Đăng nhập</a></li>
                    <li class="page_menu_item menu_mm"><a href="{{route('dangky')}}">Đăng ký</a></li>
                @else
                    <li class="page_menu_item menu_mm"><a href="#">{{$user->username}}</a></li>
                    <li class="page_menu_item menu_mm"><a href="{{route('quanlytk.update')}}">Quản lý tài khoản</a></li>
                    <li class="page_menu_item menu_mm"><a href="{{route('dangxuat')}}">Đăng xuất</a></li>
                @endif
            </ul>
        </div>
    </div>
</div>