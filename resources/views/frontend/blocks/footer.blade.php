<div class="parallax">
    <div class="container">
        <div class="row pt">
            <div class="col-lg-3 col-md-6 p-10">
                <div class="footer-title">
                    <p>Kết nối với NTK LAND</p>
                </div>
                <div class="footer-social">
                    <a href="https://www.facebook.com" title="facebook.com" target="_blank"><i class="fab fa-facebook icon"></i></a>
                    <a href="https://www.youtube.com" title="youtube.com" target="_blank"><i class="fab fa-youtube icon"></i></a>
                    <a href="https://zalo.me/pc" target="_blank" title="zalo"><img src="{{asset('frontend/img/zalo.png')}}" style="width:40px;margin-bottom:0.5em" class="icon"></a>
                </div>
                <div class="footer-title">
                    <p>Đăng ký nhận bản tin</p>
                </div>
                <form class="form-send-email">
                    <input type="email" id="email" placeholder="Địa chỉ email" name="email">
                    <button type="submit" id="bt-email">Gửi</button>
                </form>
            </div>
            <div class="col-lg-3 col-md-6 p-10">
                <div class="footer-title">
                    <p>LIÊN KẾT HỮU ÍCH</p>
                </div>       
                <ul class="footer-menu">
                  <li><a href="{{route('gioi-thieu')}}">Giới Thiệu</a></li>
                  <li><a href="{{route('contact-company')}}">Liên hệ</a></li>
                  <li><a href="{{route('project')}}">Dự án</a></li>
                </ul>  
            </div>
            <div class="col-lg-3 col-md-6 p-10">
                <div class="footer-title">
                    <p>Lượng truy cập trang</p>
                </div>
                <div class="footer-hit">
                    <p><i class="fas fa-users mr-1" style="color:#7cff99"></i>Đang truy cập :{{ $query_result_person[0]}} </p>
                    {{-- <p><i class="fas fa-chart-line mr-1" style="color:#f7133d"></i>Truy cập hôm nay :{{ $query_result_person[1]}} </p> --}}
                    <p><i class="fas fa-chart-pie mr-1" style="color:#f7e013"></i>Tổng truy cập :{{ $query_result_person[2]}}</p>
                    <p><i class="fas fa-building mr-1" style="color:#046a70"></i>Tổng dự án : {{$count_duan}}</p>
                    <p><i class="fas fa-newspaper mr-1" style="color:#b5bc96"></i>Tổng tin đăng :{{$count_tindang}}</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 p-10">
                <div class="footer-title">

                    <p>{!! $title->content !!}</p>

                </div>
                <div class="address">
                    <ul>
                        <li>

                            <p><i class="fas fa-location-arrow pr-2"></i>{!! $address->content !!}</p>

                        </li>
                        <li>

                            <p><i class="fas fa-mobile-alt pr-2"></i>{!! $hotline->content !!}</p>

                        </li>
                        <li>

                            <p><i class="fab fa-chrome pr-2"></i>{!! $email->content !!}</p>

                        </li>
                        <li>

                            <p><i class="fas fa-link pr-2"></i>{!! $baseUrl->content !!}</p>

                        </li>
                    </ul>
                </div>
            </div>

        </div>
        <!--Start of Tawk.to Script-->
        <script type="text/javascript">
            var Tawk_API = Tawk_API || {},
                Tawk_LoadStart = new Date();
            (function() {
                var s1 = document.createElement("script"),
                    s0 = document.getElementsByTagName("script")[0];
                s1.async = true;
                s1.src = 'https://embed.tawk.to/5cedeb012135900bac12f549/default';
                s1.charset = 'UTF-8';
                s1.setAttribute('crossorigin', '*');
                s0.parentNode.insertBefore(s1, s0);
            })();
        </script>
        <!--End of Tawk.to Script-->
        <div class="copy-right">
            <p> &copy;2019 Design by <a href="https://cloudone.vn" target="_blank">CloudOne</a></p>
        </div>
    </div>
</div>