<!DOCTYPE html>
<html lang="en">

<head>
    @include('frontend.blocks.head')
    <script src='https://www.google.com/recaptcha/api.js' async defer></script>
</head>

<body>
    <!-- Mở đầu và menu -->
    @include('frontend.blocks.menu')
    <div class="wrap-login ">
        <div class="container">
            <div class="login-backgroud" style="min-height: 700px;">
                <div class="login-form">
                    <!-- dăng nhập -->
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @include('admin.blocks.alert')
                    <form id="dangnhap" action="{{route('dangnhap')}}" method="POST" >
                        @csrf

                        <div class="form-group">
                            <i class="fas fa-envelope-open"></i>
                            <label>Địa chỉ email</label>
                            <input type="email" class="form-control" name="email" value="{{old('email')}}" placeholder="Nhập địa chỉ email" required="" />
                        </div>
                        <div class="form-group">
                            <i class="fas fa-key"></i>
                            <label>Mật khẩu</label>
                            <input type="password" class="form-control" name="password" placeholder="Nhập mật khẩu" required="" />
                        </div>
                        <div class="form-group form-check">
                            <input type="checkbox" class="form-check-input" name="remember" {{ old('remember') ? 'checked' : '' }} />
                            <label class="form-check-label">Nhớ tài khoản</label>
                        </div>
                        <div class="form-group">
                            <div class="g-recaptcha" data-sitekey="6LeoSL4UAAAAAM90oKyhO2Akc3OMLAiA4AuoGPHt"></div>
                        </div>
                        <button type="submit" class="btn btn-primary">Đăng nhập</button>
                        <div class="form-group pt-5">
                            <label class="form-check-label"><a href="{{ route('password.request') }}">Quên mật khẩu?</a></label>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <!-- Footer -->
    <footer class="footer">
        @include('frontend.blocks.footer')
    </footer>
 @include('frontend.blocks.lienhe')
</body>

</html>