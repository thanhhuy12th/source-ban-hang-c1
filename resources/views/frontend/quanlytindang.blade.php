<!DOCTYPE html>
<html lang="en">

<head>
    @include('frontend.blocks.head')
</head>

<body>
    @include('frontend.blocks.menu')

        @include('admin.blocks.alert')
        
        <div class="container">
            <form class="form-validate-jquery" action="{{route('quanlytk.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <fieldset class="mb-3">
                    <div class="page-header page-header-light mt-3">
                        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                            <div class="d-flex">
                                <div class="bread-crumb">
                                    <span class="breadcrumb-item"><i class="fas fa-home mr-1"></i> <a href="{{url('/')}}">Trang Chủ</a></span>
                                    <span class="breadcrumb-item"><a href="{{url('quan-ly-tai-khoan')}}">Quản lý tin đăng</a></span>
                                    <span class="breadcrumb-item">Thêm Bài Viết</span>
                                </div>
                            </div>
                        </div>
                    </div>    
                    <!-- Basic select -->
                    <div class="form-group row mt-5">
                        <label class="col-form-label col-lg-3">{{trans('template.OptionPostings')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <select name="category_id" class="form-control">
                                @for($i = 0; $i < count($cate_option); $i++) 
                                    <option value="{!!$cate_option[$i]['id']!!}" {{ (old('category_id') == $cate_option[$i]['name'])? 'selected': ''}}>{!!$cate_option[$i]['name']!!}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                    <!-- /basic select -->

                    <!-- Basic title input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Title Post')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="title" class="form-control" value="{{old('title')}}" placeholder="Tiêu đề tin đăng" required="" />
                        </div>
                    </div>
                    <!-- /basic title input -->

                     <!-- Styled file uploader -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Image Post')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="file" name="image" class="form-control" >
                        </div>
                    </div>
                    <!-- /styled file uploader -->

                    <!-- Basic alias input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Location')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="vitri" class="form-control" value="{{old('vitri')}}" placeholder="Địa chỉ" />
                        </div>
                    </div>
                    <!-- /basic alias input -->

                    <!-- Basic select -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Province')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <select id="province" name="province" class="form-control">
                                <option value="-1">-- Chọn Tỉnh Thành --</option>

                            </select>
                        </div>
                    </div>
                    <!-- /basic select -->

                    <!-- Basic select -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.District')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <select id="district" name="district" class="form-control">
                                <option value="-1">-- Chọn Quận Huyện --</option>
                            </select>
                        </div>
                    </div>
                    <!-- /basic select -->

                    <!-- Basic content input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-12">{{trans('template.Content')}}</label>
                    </div>

                    <hr>

                    <div class="card-body">
                        <ul class="nav nav-tabs">
                            <li class="nav-item"><a href="#basic-tab1" class="nav-link active show" data-toggle="tab">{{trans('template.Intro Post')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab2" class="nav-link" data-toggle="tab">{{trans('template.Acreage Content')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab3" class="nav-link" data-toggle="tab">{{trans('template.Price Content')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab4" class="nav-link" data-toggle="tab">{{trans('template.Overview')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab5" class="nav-link" data-toggle="tab">{{trans('template.Contact')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab6" class="nav-link" data-toggle="tab">{{trans('template.Location Post')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab7" class="nav-link" data-toggle="tab">{{trans('template.Image Post')}}</a></li> 
                        </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade active show" id="basic-tab1">
                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name"></label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <textarea rows="1" name="intro" class="form-control col-md-12 col-xs-12" required="">{{old('intro')}}</textarea>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="basic-tab2">
                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name"></label>
                             <div class="row">
                                <div class="col-lg-6">
                                    
                                    <div class="input-group">
                                        <input type="text" name="dientichmatbang" class="form-control" value="{{old('dientichmatbang')}}" placeholder="Diện tích mặt bằng" />
                                        <span class="input-group-append">
                                            <span class="input-group-text">m²</span>
                                        </span>
                                    </div>
                                </div>
                                 <div class="col-lg-6">

                                    <div class="input-group">
                                        <input type="text" name="dientichsudung" class="form-control" value="{{old('dientichsudung')}}" placeholder="Diện tích sử dụng" />
                                        <span class="input-group-append">
                                            <span class="input-group-text">m²</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="tab-pane fade" id="basic-tab3">
                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name"></label>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">$</span>
                                        </span>
                                        <input type="text" name="price" class="form-control" value="{{old('price')}}" placeholder="Giá bán" />
                                        <span class="input-group-append">
                                            <span class="input-group-text">.00</span>
                                        </span>
                                    </div>
                                </div>
                               
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select class="form-control form-control" name="typegia">
                                            <option value="1">VND</option>
                                            <option value="2">USD</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="basic-tab4">
                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name"></label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <textarea rows="6" autocomplete="off" id="myTextArea"  name="tongquan" class="form-control col-md-12 col-xs-12">{{ old('tongquan') }}</textarea>
                                
                            </div>
                        </div>

                        <div class="tab-pane fade" id="basic-tab5">
                            <label class="control-label col-md-12 col-sm-12 col-xs-12 mt-1" for="last-name"></label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group row">
                                    <label class="col-form-label col-lg-2">Tên liên hệ:</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="name_contact" class="form-control" placeholder="Nhập tên" />
                                    </div>
                                </div>
                                  <div class="form-group row">
                                    <label class="col-form-label col-lg-2">Email:</label>
                                    <div class="col-lg-8">
                                        <input type="email" name="email_contact" class="form-control" id="email" placeholder="Email" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-lg-2">Số điện thoại:</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="phone_contact" class="form-control" placeholder="Số điện thoại" />
                                    </div>
                                </div>
                                 <div class="form-group row">
                                    <label class="col-form-label col-lg-2">Zalo:</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="zalo_contact" class="form-control" placeholder="Zalo" />
                                    </div>
                                </div>
                               
                            </div>
                        </div>


                        <div class="tab-pane fade" id="basic-tab6">
                             <div class="container mt-3">
                                  
                                  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" style="padding: 0px 20px; font-size: 14px; border-radius: 2rem;">Hướng dẫn nhúng iframe Google Maps</button>
                                  <!-- Modal -->
                                  <div class="modal fade" id="myModal" role="dialog">
                                    <div class="modal-dialog" style="top:110px">
                                    
                                      <!-- Modal content-->
                                      <div class="modal-content">
                                        <div class="modal-header">
                                         
                                          <h4 class="modal-title">Hướng dẫn nhúng iframe google map</h4>
                                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <h6>Bước 1: Đăng nhập và truy cập vào website Google Maps tại địa chỉ: <a href="https://www.google.com/maps/preview" target="_blank" />https://www.google.com/maps/preview</a></h6>
                                                <p> - Nhập địa chỉ của bạn tại ô tìm kiếm bản đồ.</p>
                                            <h6>Bước 2: Click vào menu để lấy mã code</h6>
                                                <p> - Click vào menu bản đồ Google Maps</p>
                                                <p> - Click vào mục Chia sẻ & Nhúng bản đồ – Share & Embed map để lấy mã code</p>
                                            <h6>Bước 3: Lấy mã code nhúng vào website</h6>
                                                <p> - Click vào mục Nhúng bản đồ – Tab Embel maps</p>
                                                <p> - Copy mã code Google Maps nhúng code vào website</p>
                                        </div>
                                        <div class="modal-footer">
                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                      </div>                                      
                                    </div>
                                  </div>                                  
                                </div>
                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name"></label>
                            <div class="col-md-12 col-sm-12 col-xs-12">                                 
                               <textarea rows="6"  name="vitridacdia" class="form-control col-md-12 col-xs-12">{{old('vitridacdia')}}</textarea>
                            </div>
                        </div>  

                         <div class="tab-pane fade" id="basic-tab7">
                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name"></label>
                             <p style="color: red">Ghi chú: Chỉ cho phép định dạng jpg, png, jpeg, gif, svg và kích thước tối đa tệp tin là 2MB.</p>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                               <input type="file" name="hinh[]" class="form-input-styled" >
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                               <input type="file" name="hinh[]" class="form-input-styled" >
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                               <input type="file" name="hinh[]" class="form-input-styled" >
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                               <input type="file" name="hinh[]" class="form-input-styled" >
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                               <input type="file" name="hinh[]" class="form-input-styled" >
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                               <input type="file" name="hinh[]" class="form-input-styled" >
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                               <input type="file" name="hinh[]" class="form-input-styled" >
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                               <input type="file" name="hinh[]" class="form-input-styled" >
                            </div>
                        </div>
                    </div>
                </div>
        </div>

        <!-- /basic content input -->
        </fieldset>

        <div class="d-flex justify-content-center align-items-center mt-3">
            <button type="reset" class="btn btn-light" id="reset">{{trans('template.Reset')}}<i class="icon-reload-alt ml-2"></i></button>
            <button type="submit"  class="btn btn-primary ml-3" style="width: initial">{{trans('template.Submit')}}<i class="icon-paperplane ml-2"></i></button>
            
        </div>
        </form>
        </div>
    </section>

    <footer class="footer mt-5">
        @include('frontend.blocks.footer')
    </footer>
 @include('frontend.blocks.lienhe')
    <script type="text/javascript">
        $(document).ready(function() {
            var url = "api/province";
            $.getJSON(url, function(result) {
                $.each(result, function(i, field) {
                    var id = field.provinceid;
                    var name = field.name;
                    var type = field.type;
                    $("#province").append('<option value="' + id + '">' + type + " " + name + '</option>');
                });
            });

            //Khi chọn tỉnh thành
            $("#province").change(function() {
                $("#district").html('<option value="-1">----Chọn quận/huyện----</option>');
                var idprovince = $("#province").val();
                var url = "api/district/" + idprovince;
                $.getJSON(url, function(result) {
                    console.log(result);
                    $.each(result, function(i, field) {
                        var id = field.districtid;
                        var name = field.name;
                        var type = field.type;
                        $("#district").append('<option value="' + id + '">' + type + " " + name + '</option>');
                    });
                });
            });
        })
        $(function() {
            CKEDITOR.replace('editor1', {
                filebrowserBrowseUrl: '{{ asset('ckfinder / ckfinder.html ') }}',
                filebrowserImageBrowseUrl: '{{ asset('ckfinder / ckfinder.html ? type = Images ') }}',
                filebrowserFlashBrowseUrl : '{{ asset('ckfinder / ckfinder.html ? type = Flash ') }}',
                filebrowserUploadUrl : '{{ asset('ckfinder / core / connector / php / connector.php ? command = QuickUpload & type = Files ') }}',
                filebrowserImageUploadUrl : '{{ asset('ckfinder / core / connector / php / connector.php ? command = QuickUpload & type = Images ') }}',
                filebrowserFlashUploadUrl : '{{ asset('ckfinder / core / connector / php / connector.php ? command = QuickUpload & type = Flash ') }}'
            });
            $('.textarea').wysihtml5()
        })
        $(function() {
            CKEDITOR.replace('editor2', {
                filebrowserBrowseUrl: '{{ asset('ckfinder / ckfinder.html ') }}',
                filebrowserImageBrowseUrl: '{{ asset('ckfinder / ckfinder.html ? type = Images ') }}',
                filebrowserFlashBrowseUrl : '{{ asset('ckfinder / ckfinder.html ? type = Flash ') }}',
                filebrowserUploadUrl : '{{ asset('ckfinder / core / connector / php / connector.php ? command = QuickUpload & type = Files ') }}',
                filebrowserImageUploadUrl : '{{ asset('ckfinder / core / connector / php / connector.php ? command = QuickUpload & type = Images ') }}',
                filebrowserFlashUploadUrl : '{{ asset('ckfinder / core / connector / php / connector.php ? command = QuickUpload & type = Flash ') }}'
            });
            $('.textarea').wysihtml5()
        })


      
    </script>
</body>

</html>