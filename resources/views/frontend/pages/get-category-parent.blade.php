@extends('frontend.postings')
@section('content')
<!-- Product -->
<section class="product">
    <div class="contanier">
        <div class="row">
            <div class="col-md-4 col-lg-4 col-xl-4">
                <div class="control-bar">
                    <p class="p-title">Tìm kiếm</p>
                    <div class="search-product">
                        <div class="container">
                              <form id="search" action="{{route('timkiem')}}" method="POST">
                                @csrf
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="select-box">
                                            <div class="select-title">
                                                <p>Danh mục</p>
                                            </div>
                                            <div class="select-content">
                                               <select name="danhmuc">
                                                   @for($i = 0; $i < count($danhmuc); $i++)
                                                    <option value="{!!$danhmuc[$i]['id']!!}" {{ (old('category_id') == $danhmuc[$i]['name'])? 'selected': ''}}>{!!$danhmuc[$i]['name']!!}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <div class="select-box">
                                            <div class="select-title">
                                                <p>Tỉnh Thành</p>
                                            </div>
                                            <div class="select-content">
                                                <select  id="province"  name="province">
                                                    <option value="-1">--Chọn Tỉnh Thành--</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <div class="select-box">
                                            <div class="select-title">
                                                <p>--Quận/Huyện--</p>
                                            </div>
                                            <div class="select-content">
                                                <select  id="district"  name="district">
                                                    <option value="-1">--Chọn Quận Huyện--</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <div class="select-box">
                                            <div class="select-title">
                                                <p>Khoảng diện tích</p>
                                            </div>
                                            <div class="select-content">
                                                <select name="dientich">
                                                    <option value="-1">--Chọn diện tích--</option>
                                                    <option value="less-15">< 15 m<sup>2</sup></option>
                                                    <option value="15-25">15 m<sup>2</sup> - 25m<sup>2</sup></option>
                                                    <option value="25-50">25 m<sup>2</sup> - 50m<sup>2</sup></option>
                                                    <option value="50-100">50 m<sup>2</sup> - 100<sup>2</sup></option>
                                                    <option value="more-100">> 100 m<sup>2</sup></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <div class="select-box">
                                            <div class="select-title">
                                                <p>Khoảng giá</p>
                                            </div>
                                            <div class="select-content">
                                                <select name="price">
                                                    <option value="-1">--Chọn giá--</option>
                                                    <option value="less-1m">< 1 triệu</option>
                                                    <option value="1m-3m"> 1 triệu - 3 triệu</option>
                                                    <option value="3m-5m"> 3 triệu - 5 triệu</option>
                                                    <option value="5m-10m"> 5 triệu - 10 triệu</option>
                                                    <option value="10m-100m"> 10 triệu - 100 triệu</option>
                                                    <option value="100m-1b"> 100 triệu - 1 tỷ</option>
                                                    <option value="1b-10b"> 1 tỷ - 10 tỷ</option>
                                                    <option value="more-10b"> > 10 tỷ</option>
                                                    
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="search_button_container" style="margin-left:17%">
                                        <input type="submit" name="sumbit" value="Tìm kiếm">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="control-bar mt-20">
                    <p class="p-title">Danh mục</p>
                    <ul>
                        <?php
                        $menu_lv_1 = DB::table('bds_dmtd')->where('parent_id', 0)->get();
                        ?>
                        @foreach($menu_lv_1 as $item_lv_1)
                        <li>
                            <a href="{{url('product-category-parent',[$item_lv_1->alias])}}">{{$item_lv_1->name}}</a>
                            <ul>
                                <?php
                                $menu_lv_2 = DB::table('bds_dmtd')->where('parent_id', $item_lv_1->id)->get();
                                ?>
                                @foreach($menu_lv_2 as $item_lv_2)
                                <li class="left-sub">
                                    <a href="{{url('product-category',[$item_lv_2->alias,$item_lv_2->parent_id])}}">-- {{$item_lv_2->name}}</a>
                                </li>
                                @endforeach
                            </ul>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <div class="control-bar mt-20">
                    <p class="p-title">Bài viết liên quan</p>
                    <div class="h-title">
                        @foreach($product_category as $item)
                        <p><a href="{{route('product-detail',[$item->alias])}}">{{$item->title}}</a>
                        </p>
                        @endforeach
                    </div>
                </div>

                <div class="control-bar mt-20 mb-20">
                    <p class="p-title">Thông tin dự án</p>
                    <div class="h-title">
                        @foreach($duan as $item)
                        <p><a href="{{route('project-detail',[$item->alias])}}">{{$item->name}}</a></p>
                        @endforeach
                    </div>
                </div>

            </div>
            <div class="col-md-8 col-lg-8 col-xl-8">
                <?php
                        //pagi
                            $result=get_category_parent($alias);
                            $row_per_page=8 ;
                            $rows=count($result);
                            if ($rows>$row_per_page) $page=ceil($rows/$row_per_page);
                            else $page=1;
                            if(isset($_GET['start']) && (int)$_GET['start'])
                                $start=$_GET['start'];
                            else
                                $start=0;
                            $result=get_category_parent_limit($alias,$start,$row_per_page);
                                //End pagi
                    ?>
                @foreach($result as $item)
                    @if($item->duyetbai ==1 && $item->status ==1)
                        <div class="row pb-4">
                            <div class="col-lg-12 col-md-12">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4">
                                        <div class="alter-grid">
                                            <img src="{{asset('uploads/postings/'.$item->image)}}">
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-md-8">
                                        <div class="grid">
                                            <div class="grid-title">
                                                <a href="{{route('product-detail',[$item->alias])}}">
                                                    <p>{!! mb_strtolower($item->title) !!}</p>
                                                </a>
                                            </div>
                                            <div class="grid-content">
                                                <p>{!! $item->intro !!}</p>
                                            </div>
                                        </div>
                                        <div class="all">
                                            <div class="meta-item gia">
                                                <span style="color: red">Giá:</span> <span><span class="meta-item-gia">{!! number_format($item->price,0,",",".") !!}</span> </span>
                                            </div>

                                            <div class="meta-item dien_tich">
                                                <span style="color: red"> Diện tích:</span> <span><span class="meta-item-dien-tich">{!! $item->dientichsudung."m²" !!} </span> </span>
                                            </div>
                                            <div class="meta-item vi_tri">
                                                <span style="color: red">Khu vực:</span> <span class="meta-item-vi-tri">{!! $item->vitri !!}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    <hr>
                @endforeach
                <div class="row" style="text-align: center; margin-top: 25px; margin-bottom: 20px">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        @php
                            $page_cr=($start/$row_per_page)+1;
                            for($i=1;$i<=$page;$i++)
                            {
                                if ($page_cr!=$i) echo "<div style='margin-left: 1px;' class='btn-group'>"."<a href='".$alias."?start=".$row_per_page*($i-1)."'><button class='btn btn-info' style='background-color: #007bff'>$i</button></a>"."</div>";
                                else echo "<div style='margin-left: 1px;' class='btn-group'><button style='background-color: #fb4908ba; border-color: #fb4908ba' class='btn btn-info active'>".$i." "."</button></div>";
                            }
                        @endphp
                    </div>
                    <div class="col-md-4"></div>
                </div>
            </div>
                
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function(){
        var url = "../../api/province";
        $.getJSON(url,function(result){
            $.each(result, function(i, field){
                var id      = field.provinceid;
                var name    = field.name;
                var type    = field.type;
                $("#province").append('<option value="'+id+'">'+" "+name+'</option>');
            });
        });

        //Khi chọn tỉnh thành
        $("#province").change(function() {
            $("#district").html('<option value="-1">----Chọn quận/huyện----</option>');
            var idprovince = $("#province").val();
            var url = "../../api/district/"+idprovince;
            $.getJSON(url,function(result){
                console.log(result);
                $.each(result, function(i, field){
                    var id      = field.districtid;
                    var name    = field.name;
                    var type    = field.type;
                    $("#district").append('<option value="'+id+'">'+" "+name+'</option>');
                });
            });
        });
    })
</script>
@endsection