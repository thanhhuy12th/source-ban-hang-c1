
<div class="row">
    <div class="col-md-12 col-lg-6">
        <div class="contact-company" id="lienhe">
            @if(Session::has('success'))
            <div class="alert alert-success" id="flash-message">
                {{ Session::get('success') }}
            </div>
            @endif
            <div class="contact-company-title">
                <span>Liên hệ ngay với chúng tôi</span>
                <small id="Help" class="form-text text-muted">Công ty tuyệt đối tôn trọng quyền riêng tư của
                    khách hàng,
                    chúng tôi KHÔNG công khai hoặc sử dụng thông tin liên lạc cho mục đích khác.</small>
            </div>
            <form action="{{route('lien-he.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group fullname">
                    <label>Name</label>
                    <input type="text" class="form-control" name="name" id="contact-company-name" placeholder="Nhập Tên" value="{{old('name')}}" required="">
                    <div class="alert alert-warning">
                        <strong>Tên đăng ký của bạn đã vượt quá 50 ký tự</strong>
                    </div>
                </div>
                 <div class="form-group fullname">
                    <label>Phone</label>
                    <input type="text" class="form-control" name="phone" id="contact-company-name" placeholder="Số điện thoại" value="{{old('phone')}}" required="">
                </div>
                <div class="form-group">
                    <label>Địa chỉ email</label>
                    <input type="email" class="form-control" name="email" id="contact-company-email" aria-describedby="emailHelp" placeholder="john@example.com" value="{{old('email')}}" required="">
                </div>

                <div class="form-group mess">
                    <label for="message">Message</label>
                    <textarea type="text" class="form-control luna-message" id="message" placeholder="Gửi nội dung" name="content" required="">{{old('content')}}</textarea>
                    <div class="alert alert-warning">
                        <strong>Tên đăng ký của bạn đã vượt quá 200 ký tự</strong>
                    </div>
                    <div class="alert alert-info">
                        <strong>Có ký tự đặc biệt</strong>
                    </div>
                </div>
                <div class="form-group">
                    <div class="g-recaptcha" data-sitekey="6LeoSL4UAAAAAM90oKyhO2Akc3OMLAiA4AuoGPHt"></div>
                </div>
                <button type="submit" class="btn btn-primary">Gửi thông tin</button>
            </form>
        </div>
    </div>
    <div class="col-md-12 col-lg-6">
        <div class="map">
        {!! $ggmap->content !!}
        </div>
    </div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDFdq9oHzzD1CvF0qICpfWRI0B5RnKyX7E&callback=initMap" async defer></script>