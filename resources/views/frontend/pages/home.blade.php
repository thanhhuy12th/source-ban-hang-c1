@extends('frontend.index')
@section('content')
<!-- Slide -->
<section class="slide">
            
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php $i = 0; ?>
            @foreach($slider as $i=>$value)
            <li data-target="#carousel-example-generic" data-slide-to="{{$i}}" @if($i==0) class="active" @endif></li>
            @endforeach
            <?php $i++; ?>
        </ol>
        <div class="carousel-inner" role="listbox">
            <?php $i = 0; ?>
            @foreach($slider as $sl)
            <div @if($i==0) class="carousel-item active" @else class="carousel-item" @endif>
                <?php $i++; ?>
                <img class="d-block w-100" src="{{asset('uploads/slider/'.$sl->image)}}" alt="{{$sl->intro}}">
            </div>
            @endforeach
        </div>
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="icon-prev" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="icon-next" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>

<!-- Tìm kiếm -->
<section class="search">
    <div class="container">
        <div class="sec-title">
            <div class="m-title">
                <p>TÌM KIẾM NGAY BẤT ĐỘNG SẢN CỦA BẠN</p>

            </div>
            <div class="m-content">
                <p>Nhiều bất động sản đang bán giá hấp dẫn</p>
            </div>
        </div>
        <!-- combox search -->
        <form id="search" action="{{route('timkiem')}}" method="POST">
            @csrf
            <div class="row">
                <div class="col-lg">
                    <div class="select-box">
                        <div class="select-title">
                            <p>Danh mục</p>
                        </div>
                        <div class="select-content">
                            <select name="danhmuc">
                                @for($i = 0; $i < count($danhmuc); $i++) <option value="{!!$danhmuc[$i]['id']!!}" {{ (old('category_id') == $danhmuc[$i]['name'])? 'selected': ''}}>{!!$danhmuc[$i]['name']!!}</option>
                                    @endfor
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg">
                    <div class="select-box">
                        <div class="select-title">
                            <p>Tỉnh Thành</p>
                        </div>
                        <div class="select-content">
                            <select id="province" name="province">
                                <option value="-1">--Chọn Tỉnh Thành--</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg">
                    <div class="select-box">
                        <div class="select-title">
                            <p>--Quận/Huyện--</p>
                        </div>
                        <div class="select-content">
                            <select id="district" name="district">
                                <option value="-1">--Chọn Quận Huyện--</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg">
                    <div class="select-box">
                        <div class="select-title">
                            <p>Khoảng diện tích</p>
                        </div>
                        <div class="select-content">
                            <select name="dientich">
                                <option value="-1">--Chọn diện tích--</option>
                                <option value="less-15">
                                    < 15 m<sup>2</sup>
                                </option>
                                <option value="15-25">15 m<sup>2</sup> - 25m<sup>2</sup></option>
                                <option value="25-50">25 m<sup>2</sup> - 50m<sup>2</sup></option>
                                <option value="50-100">50 m<sup>2</sup> - 100<sup>2</sup></option>
                                <option value="more-100">> 100 m<sup>2</sup></option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg">
                    <div class="select-box">
                        <div class="select-title">
                            <p>Khoảng giá</p>
                        </div>
                        <div class="select-content">
                            <select name="price">
                                <option value="-1">--Chọn giá--</option>
                                <option value="less-1m">
                                    < 1 triệu</option> <option value="1m-3m"> 1 triệu - 3 triệu
                                </option>
                                <option value="3m-5m"> 3 triệu - 5 triệu</option>
                                <option value="5m-10m"> 5 triệu - 10 triệu</option>
                                <option value="10m-100m"> 10 triệu - 100 triệu</option>
                                <option value="100m-1b"> 100 triệu - 1 tỷ</option>
                                <option value="1b-10b"> 1 tỷ - 10 tỷ</option>
                                <option value="more-10b"> > 10 tỷ</option>

                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="search_button_container">
                    <input type="submit" name="sumbit" value="Tìm Kiếm">
                </div>
            </div>
        </form>
    </div>
</section>
<!-- Dự án nổi bật -->
<section class="project">
    <div class="container">
        <div class="sec-title">
            <div class="m-title">
                <p>Dự án nổi bật</p>
            </div>
        </div>
        <div class="row pb-25">

            <div class="col-lg-4 col-md-4 col-12">

                <div class="banner-small">
                    <div class="block">
                        <a href="{{route('project-detail',[$duan[0]->alias])}}"><img src="{!!asset('uploads/postings/'. $duan[0]->image ) !!}"></a>
                    </div>
                    <div class="sec-banner-small-title">
                        <div class="s-title">
                            <p>{!! $duan[0]->name !!}</p>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-lg-8 col-md-8 col-12">

                <div class="banner-large">
                    <div class="block">
                        <a href="{{route('project-detail',[$duan[1]->alias])}}"><img src="{!!asset('uploads/postings/'. $duan[1]->image ) !!}"></a>
                    </div>

                    <div class="sec-banner-large-title">
                        <div class="s-title">
                            <p>{!! $duan[1]->name !!}</p>
                        </div>
                    </div>
                </div>

            </div>


        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-12">
                <div class="banner-small">
                    <div class="block">
                        <a href="{{route('project-detail',[$duan[2]->alias])}}"><img src="{!!asset('uploads/postings/'. $duan[2]->image ) !!}"></a>
                    </div>
                    <div class="sec-banner-small-title">
                        <div class="s-title">
                            <p>{!! $duan[2]->name !!} </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-12">
                <div class="banner-small">
                    <div class="block">
                        <a href="{{route('project-detail',[$duan[3]->alias])}}"><img src="{!!asset('uploads/postings/'. $duan[3]->image ) !!}"></a>
                    </div>
                    <div class="sec-banner-small-title">
                        <div class="s-title">
                            <p>{!! $duan[3]->name !!} </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-12">
                <div class="banner-small">
                    <div class="block">
                        <a href="{{route('project-detail',[$duan[4]->alias])}}"><img src="{!!asset('uploads/postings/'. $duan[4]->image ) !!}"></a>
                    </div>
                    <div class="sec-banner-small-title">
                        <div class="s-title">
                            <p>{!! $duan[4]->name !!} </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<!-- Nhà đất bán nổi bật -->
<section class="popular-project">
    <div class="container ">
        <div class="sec-title">
            <div class="m-title">
                <p>NHÀ ĐẤT BÁN NỔI BẬT</p>
            </div>
        </div>
        <div class="border-box">
            @foreach($postings as $item)
            <div class="box-wrap">
                <div class="row">
                    <div class="col-lg">
                        <div class="sec-banner-project-title">
                            <div class="b-title">
                                <a href="{{route('product-detail',[$item->alias])}}">
                                    <p>{!! $item->title !!}</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-lg-4 col-12">
                        <div class="box-image">                            
                            {!!ImagesCheck($item)!!}
                        </div>
                    </div>
                    <div class="col-lg-8 col-12">
                        <div class="row">
                            <div class="box-content">
                                <p>{!! $item->intro !!}</p>
                            </div>
                        </div>
                        <div class="row r-15">
                            <div class="col-lg-3 col-12">
                                <div class="box-text">
                                    @if($item->typegia == 1)
                                        <p><span>Giá:</span>{!! number_format($item->price,0,",",".") . " VND " !!}</p>
                                    @else
                                        <p><span>Giá:</span>{!! number_format($item->price,0,",",".") . " USD " !!}</p>
                                    @endif 
                                </div>
                            </div>
                            <div class="col-lg-3 col-12">
                                <div class="box-text">
                                    <div class="box-text">                                      
                                        <p><span>Diện tích:</span>{!!  $item->dientichsudung . " m² " !!}</p>                                 
                                    </div>  
                                </div>
                            </div>
                            <div class="col-lg-3 col-12">
                                <div class="box-text">
                                    <p><span>Vị trí:</span>{!! $item->vitri !!}</p>
                                </div>
                            </div>
                            <div class="col-lg-3 col-12">
                                <div class="box-text">
                                    <p><span>Ngày cập nhật:</span>{!! $item->created_at !!}</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            @endforeach
        </div>
        {{-- <div class="row">
            <div class="search_button_container">
                <a href="{{url('product-category-parent'}}">
                    <button type="button" class="btn btn-primary">Xem thêm</button>
                </a>
            </div>
        </div> --}}
    </div>
</section>
<!-- Dụ án nổi bật -->
<section class="hot-project">
    <div class="container">
        <div class="sec-title">
            <div class="m-title">
                <p>DỰ ÁN NỔI BẬT</p>
            </div>
        </div>
        <div class="border-box border-box-blue">
            <div class="row">
                @foreach($duannoibat as $item)
                <div class="col-lg-3 col-md-6">
                    <div class="grid-image">

                        <img src="{{asset('uploads/postings/'.$item->image)}}">
                    </div>
                    <div class="grid-title pt-20 height-45">
                        <a href="{{route('project-detail',[$item->alias])}}">
                            <p>{!! $item->name !!}</p>
                        </a>
                    </div>
                    <div class="grid-content pt-20">
                        <p>{!! $item->intro !!}</p>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </div>
</section>

<!-- Tin tức sự kiện -->
<section class="news">
    <div class="container ">
        <div class="sec-title">
            <div class="m-title">
                <p>TIN TỨC</p>
            </div>
        </div>
        <div class="border-box border-news">
            <div class="row">
                @foreach($news as $item)
                <div class="col-lg-3">
                    <div class="grid-image">
                        <img src="{{asset('uploads/postings/'.$item->image)}}">
                    </div>
                    <div class="grid-title pt-20">
                        <a href="{{route('news-detail',[$item->alias])}}">
                            <p>{!! $item->title !!}</p>
                        </a>
                    </div>
                    <div class="grid-content">
                        <p>{!! $item->intro !!}</p>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </div>
</section>

<!-- Gio thieu -->
<section class="about">
    <div class="container">
        <div class="sec-title">
            <div class="m-title">
                <p>GIỚI THIỆU VỀ NTK LAND</p>
            </div>
        </div>

        <div class="border-box">
            <div class="row">
                <div class="col-md-6 col-lg-6 col-xl-6">
                    <div class="slider-contanier">
                        <div class="slider">
                            @foreach($slider as $sl)
                            <img src="{{asset('uploads/slider/'.$sl->image)}}">
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-xl-6">
                    <div class="about-title">
                        <p>NTK LAND luôn vững vàng với mục tiêu mang lại cho khách hàng những sản phẩm bất động sản tốt nhất bằng dịch vụ chuyên nghiệp nhất.</p>
                    </div>
                    <div class="history-content">
                        <p>Công ty Cổ phần được thành lập từ năm 2009. Sau gần 10 năm hình thành và phát triển, đến nay NTK LAND đã khẳng định được uy tín và năng lực cạnh tranh hàng đầu trong ngành tư vấn, phân phối Bất động sản tại Việt Nam. Với tôn chỉ hoạt động: Trọn chữ TÍN, vẹn niềm TIN, chúng tôi đã nỗ lực trở thành kênh tư vấn tin cậy, mang đến cho khách hàng những dự án, sản phẩm đầu tư chất lượng và tiềm năng sinh lời cao nhất.</p>
                    </div>
                </div>

            </div>

        </div>
    </div>
</section>

<!-- Linh vuc hoat dong -->
<section class="end">
    <div class="container">
        <div class="sec-title">
            <div class="m-title">
                <p>LĨNH VỰC HOẠT ĐỘNG</p>
            </div>
        </div>
        <div class="end-container">
            <div class="row">
                <div class="col-lg-4 col-md-4 end-border">
                    <div class="end-image">
                        <img src="{{asset('uploads/xaydung.png')}}">
                    </div>
                    <div class="end-title">
                        <p>Xây dựng</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 end-border">
                    <div class="end-image">
                        <img src="{{asset('uploads/taichinh.png')}}">
                    </div>
                    <div class="end-title">
                        <p>Đầu tư - Tài chính </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 end-border">
                    <div class="end-image">
                        <img src="{{asset('uploads/bds.png')}}">
                    </div>
                    <div class="end-title">
                        <p>Bất động sản </p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<section class="bar background-pentagon no-mb">
    <div class="container">
        <div class="sec-title">
            <div class="m-title">
                <p>LƯỢNG TRUY CẬP</p>
            </div>
        </div>
        <div class="hit-container">
            <div class="row">
                <div class="col-lg-4 col-md-4 end-border">
                    <div class="icon-hit">
                        <i class="fas fa-users fadein"></i>
                    </div>
                    <div class="hit-title">
                        <p>Đang truy cập</p>
                        <p>{{ $query_result_person[0]}}<p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 end-border hit">
                    <div class="icon-hit">
                        <i class="fas fa-chart-line fadein"></i>
                    </div>
                    <div class="hit-title">
                        <p>Dự án</p>
                        <p>{{ $count_duan }}<p>
                    </div>
                </div>
                {{-- <div class="col-lg-3 col-md-3 end-border">
                    <div class="icon-hit">
                        <i class="fas fa-chart-bar fadein"></i>
                    </div>
                    <div class="hit-title">
                        <p>Lượng truy cập tuần rồi</p>
                        <p>45324<p>
                    </div>
                </div> --}}
                <div class="col-lg-4 col-md-4 end-border">
                    <div class="icon-hit">
                        <i class="fas fa-chart-pie rotation"></i>
                    </div>
                    <div class="hit-title">
                        <p>Tổng lượng truy cập</p>
                        <p>{{ $query_result_person[2]}}<p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Script -->
<script type="text/javascript">
    $(document).ready(function() {
        var url = "./api/province";
        $.getJSON(url, function(result) {
            $.each(result, function(i, field) {
                var id = field.provinceid;
                var name = field.name;
                var type = field.type;
                $("#province").append('<option value="' + id + '">' + " " + name + '</option>');
            });
        });

        //Khi chọn tỉnh thành
        $("#province").change(function() {
            $("#district").html('<option value="-1">----Chọn quận/huyện----</option>');
            var idprovince = $("#province").val();
            var url = "./api/district/" + idprovince;
            $.getJSON(url, function(result) {
                console.log(result);
                $.each(result, function(i, field) {
                    var id = field.districtid;
                    var name = field.name;
                    var type = field.type;
                    $("#district").append('<option value="' + id + '">' + " " + name + '</option>');
                });
            });
        });
    })
</script>
@endsection