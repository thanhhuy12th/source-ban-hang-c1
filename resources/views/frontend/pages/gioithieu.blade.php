@extends('frontend.about') 
@section('content')
    <!-- Home icon -->
    <section class="home-icon">
        <div class="container">
            <div class="home-icon-containner">
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="icon-image"><i class="fas fa-landmark"></i></div>
                        <div class="icon-title">Lịch sử hình thành và phát triển</div>
                        <div class="icon-content">hông ngừng hoàn thiện, cải tiến nhằm phát triể
                            n các dự án Bất Động Sản vì mục tiêu an sinh xã hội</div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="icon-image"><i class="fas fa-chart-line"></i></div>
                        <div class="icon-title">Tầm nhìn</div>
                        <div class="icon-content">Trở thành một trong những Nhà tư vấn,
                            phân phối Bất Động Sản chuyên nghiệp và uy tín nhất tại Việt Nam</div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="icon-image"><i class="fas fa-city"></i></div>
                        <div class="icon-title">Sứ mệnh</div>
                        <div class="icon-content">Trở thành một trong những Nhà tư vấn,
                            phân phối Bất Động Sản chuyên nghiệp và uy tín nhất tại Việt Nam</div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="icon-image"><i class="fas fa-gem"></i></div>
                        <div class="icon-title">Gía trị</div>
                        <div class="icon-content">Trở thành một trong những Nhà tư vấn,
                            phân phối Bất Động Sản chuyên nghiệp và uy tín nhất tại Việt Nam</div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <!-- lịch sử công ty -->
    <section class="history">
        <div class="container">
            {!! $gioithieu->content !!}
            <p style="color: red">{!! $title->content !!}</p>
            <p><i class="fas fa-map-marker-alt pr-2"></i>{!! $address->content !!}</p>
            <p><i class="fas fa-phone-volume pr-2"></i>{!! $hotline1->content !!}</p>
            <p><i class="far fa-envelope pr-2"></i>{!! $email->content !!}</p>
            <p><a href="{!! $link->content !!}"><i class="fas fa-globe-asia pr-2"></i>{!! $link->content !!}</a></p>

        </div>
    </section>

    <!-- Xem video -->
    <section class="video">
        <div class="container">
            
            <div class="video-container">
                {!! $video->linkyoutube !!}
            </div>
            
        </div>
        <script>

        </script>
    </section>

    <!-- Thông tin liên hệ -->
    <section class="contact">
        <div class="container contact-containner">
        @include('frontend.pages.contactUs')
        </div>
    </section>
@endsection