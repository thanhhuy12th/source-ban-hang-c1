@extends('frontend.news') 
@section('content')
    <!-- New -->
    <section class="product">
        <div class="contanier">
            <div class="row">
                <div class="col-md-8 col-lg-8">
                    @foreach($news as $tt)
                    <div class="row pb-4">
                        <div class="col-lg-4 col-md-4">
                            <div class="alter-grid">
                                <img src="{{asset('uploads/postings/'.$tt->image)}}">
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-8">
                            <div class="grid-date">
                                <i class="fas fa-calendar-alt"></i>
                                {{$tt->created_at}}
                            </div>
                            <div class="grid-title pt-10">
                                <a href="{{route('news-detail',[$tt->alias])}}"><p>{{$tt->title}}</p></a>
                            </div>
                            <div class="grid-content">
                                <p>{!! $tt->intro !!}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <div class="row" >
                        <div class="col-md-4"></div>
                        <div class="col-md-4 pt-5">
                             <ul class="pagination" >
                                 @if( $news->currentPage() != 1)
                                    <li class="page-item" >
                                      <a class="page-link" href="{!! $news->url($news->currentPage() - 1) !!}" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                        <span class="sr-only">Previous</span>
                                      </a>
                                    </li>
                                 @endif
                                 @for($i=1; $i <= $news->lastPage(); $i= $i+1)
                                    <li class="page-item {!! ($news->currentPage() == $i )? 'active': '' !!}">
                                        <a class="page-link" href="{!! $news->url($i) !!}">{!! $i !!}</a></li>      
                                @endfor
                                @if($news->currentPage() != $news->lastPage())
                                    <li class="page-item">
                                      <a class="page-link" href="{!! $news->url($news->currentPage() + 1) !!}" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                        <span class="sr-only">Next</span>
                                      </a>
                                    </li>
                                 @endif
                              </ul>
                        </div>
                        <div class="col-md-4"></div>
                    </div>               
                      
                </div>
                <div class="col-md-4 col-lg-4">
                    <div class="control-bar">
                        <p class="p-title">Tin Mới Nhất</p>
                        <ul>
                            @foreach($newsnews as $item)
                            <li class="pb-3 pr-2">
                                <a href="{{route('news-detail',[$tt->alias])}}">{{$item->title}}</a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="control-bar mt-20">
                    <p class="p-title">Bài viết liên quan</p>
                    <div class="h-title">
                        @foreach($product_category as $item)
                        <p><a href="{{route('product-detail',[$item->alias])}}">{{$item->title}}</a>
                        </p>
                        @endforeach
                    </div>
                </div>
                </div>
            </div>
        </div>
    </section>
@endsection