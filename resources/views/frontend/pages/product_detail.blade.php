@extends('frontend.postings') 
@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('frontend/slider/dist/css/slider-pro.min.css')}}" media="screen"/>
<link rel="stylesheet" type="text/css" href="{{asset('frontend/slider/libs/fancybox/jquery.fancybox.css')}}" media="screen"/>
<link rel="stylesheet" type="text/css" href="{{asset('frontend/slider/css/examples.css')}}" media="screen"/>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>

<script type="text/javascript" src="{{asset('frontend/slider/libs/jquery-1.11.0.min.js')}}"></script>
<script type="text/javascript" src="{{asset('frontend/slider/dist/js/jquery.sliderPro.min.js')}}"></script>
<script type="text/javascript" src="{{asset('frontend/slider/libs/fancybox/jquery.fancybox.pack.js')}}"></script>

<script type="text/javascript">
    $( document ).ready(function( $ ) {
        $( '#example2' ).sliderPro({
            width: 300,
            height: 300,
            visibleSize: '100%',
            forceSize: 'Width',
            autoSlideSize: true
        });

        // instantiate fancybox when a link is clicked
        $( ".slider-pro" ).each(function(){
            var slider = $( this );

            slider.find( ".sp-image" ).parent( "a" ).on( "click", function( event ) {
                event.preventDefault();

                if ( slider.hasClass( "sp-swiping" ) === false ) {
                    var sliderInstance = slider.data( "sliderPro" ),
                        isAutoplay = sliderInstance.settings.autoplay;

                    $.fancybox.open( slider.find( ".sp-image" ).parent( "a" ), {
                        index: $( this ).parents( ".sp-slide" ).index(),
                        afterShow: function() {
                            if ( isAutoplay === true ) {
                                sliderInstance.settings.autoplay = false;
                                sliderInstance.stopAutoplay();
                            }
                        },
                        afterClose: function() {
                            if ( isAutoplay === true ) {
                                sliderInstance.settings.autoplay = true;
                                sliderInstance.startAutoplay();
                            }
                        }

                    });
                }
            });
        });
    });
</script>
{{-- <div class="btn-srcoll-to" id="mybtnsrcoll">
    <span>
         <i class="fas fa-arrow-circle-left"></i>
     </span>
</div>
<section class="scroll-to-the-selected none ">
    <div class="scroll-container">
        <ul class="main-scroll-container">
            <li class="sub-scroll-container"><i class="fas fa-globe "></i><a href="#tongquan">Tổng quan</a>
            </li>
            <li class="sub-scroll-container"><i class="fas fa-map-marker-alt "></i><a href="#vitridacdia">Vị trí</a>
            </li>
            <li class="sub-scroll-container"><i class="fas fa-map "></i><a href="#dt">Diện tích</a></li>
            <li class="sub-scroll-container"><i class="fas fa-dollar-sign "></i><a href="#giaban">Giá bán</a></li>
            <li class="sub-scroll-container"><i class="fas fa-{{asset('frontend/')}}images "></i><a href="#hinh">Hình ảnh</a></li>
            <li class="sub-scroll-container"><i class="fas fa-phone-volume "></i><a href="#contact">Liên hệ</a>
            </li>
        </ul>
    </div>
</section> --}}
<section class="product-detail">
    <div class="contanier ">
        <div class="alter-contanier ">
            <h3>{!! $cates_detail->title !!}</h3>
        </div>
        <div class="col-sm-12 row ">
            <div class="box-text" style="left: 16px;">
                <p><span>Khu vực: </span>{!! $cates_detail->vitri !!}</p>
            </div>
        </div>
        <div class="col-sm-12 row">
            <div class="col-md-4">
                 <div id="dt" >
                    <div class="box-text">
                         <p><span>Diện tích mặt bằng: </span>{!! $cates_detail->dientichmatbang . " m² " !!}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                 <div id="dt" >
                <div class="box-text">
                    <p><span>Diện tích sử dụng: </span>{!! $cates_detail->dientichsudung . " m² " !!}</p>
                </div>
            </div>
            </div>
            <div class="col-md-4">
                 <div id="giaban" >
                    <div class="box-text">
                        @if($cates_detail->typegia == 1)
                            <p><span>Giá: </span>{!! number_format($cates_detail->price,0,",",".") . " VND " !!}</p>
                        @else
                            <p><span>Giá: </span>{!! number_format($cates_detail->price,0,",",".") . " USD " !!}</p>
                        @endif                    
                    </div>
                </div>
            </div>
            
        </div>
         <div id="tongquan" style="left: 15px;">
                <h6 style="color: #0000ff; font-weight: bold">Thông tin </h6>
                <input type="hidden" id="tongquanbreak" value="{{ $cates_detail->tongquan }}">
                <p id="output2"></p>
            </div> 
           
                  <div id="contact" style="left: 5px;">
                  {{--  <h6 style="color: #0000ff">Liên hệ </h6> --}}
                    @php 
                        $contact=explode("||",$cates_detail->contact); 
                    @endphp 
                    
                        
                   
                    <table class="table">
                     
                      <tbody>
                        @for($i =0; $i<1; $i++) 
                        <tr >
                          <td style="width: 250px; color: #0000ff; font-weight: bold">Liên hệ:</td>
                          <td >{!! $contact[0] !!}</td>
                        </tr>
                        <tr>
                          <td style="color: #0000ff; font-weight: bold">Email:</td>
                          <td>{!! $contact[1] !!}</td>
                        </tr>
                        <tr>
                          <td style="color: #0000ff; font-weight: bold">Phone:</td>
                          <td>{!! $contact[2] !!}</td>
                        </tr>
                        <tr>
                          <td style="color: #0000ff; font-weight: bold">Zalo</td>
                          <td>{!! $contact[3] !!}</td>
                        </tr>
                       @endfor
                      </tbody>
                    </table>
                </div>
                <div id="hinh" style="left: 15px;">
                   <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Xem ảnh</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Xem bản đồ</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">                      
                                  <div id="example2" class="slider-pro">
                                    <div class="sp-slides">
                                         @php 
                                            $splittedstring=explode("||",$cates_detail->hinh); 
                                        @endphp
                                         @for($i =0; $i<count($splittedstring)-1; $i++)                                          
                                        <div class="sp-slide">
                                            <a href="{{asset('uploads/postings/'.$splittedstring[$i])}}">
                                                <img class="sp-image" src="{{asset('uploads/postings/'.$splittedstring[$i])}}"  
                                                data-src="{{asset('uploads/postings/'.$splittedstring[$i])}}" 
                                                data-retina="{{asset('uploads/postings/'.$splittedstring[$i])}}" /></a>
                                        </div>
                                        @endfor
                                    </div>
                                </div>                           
                        </div>
                        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab" style="text-align: center">
                            {!! $cates_detail->vitridacdia !!}
                        </div>                       
                    </div>
            </div>
    </div>
<script type="text/javascript">
    let textareaText = $('#tongquanbreak').val();
     textareaText = textareaText.replace(/\r?\n/g, '<br />');
        $('#output2').html(textareaText);

</script>
</section>
@endsection