@extends('frontend.recruitment') 
@section('content')
    <!-- New -->
    <section class="product">
        <div class="contanier">
            <div class="row">
                <div class="col-md-8 col-lg-8">
                    @foreach($tuyendung as $td)
                    <div class="row pb-4">
                        <div class="col-lg-4 col-md-4">
                            <div class="alter-grid">
                                <img src="{{asset('uploads/postings/'.$td->image)}}">
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-8">
                            <div class="grid-date">
                                <i class="fas fa-calendar-alt"></i>
                                {{$td->created_at}}
                            </div>
                            <div class="grid-title">
                                <a href="{{route('chi-tiet-tuyen-dung',[$td->alias])}}"><p>{{$td->title}}</p></a>
                            </div>
                            <div class="grid-content">
                                <p>{!! $td->intro !!}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                   
                    <ul class="pagination">
                        @if($tuyendung->currentPage() !=1)
                        <li class="page-item"><a class="page-link" href="{!! $tuyendung->url($tuyendung->currentPage() - 1) !!}">Previous</a></li>
                        @endif
                        @for($i=1 ; $i <= $tuyendung->lastPage() ; $i = $i+1)
                        <li class="page-item {!! ($tuyendung->currentPage() ==$i)? 'active':'' !!} ">
                            <a class="page-link" href="{!! $tuyendung->url($i) !!}">{!! $i !!}</a>
                        </li>
                       @endfor
                       @if($tuyendung->currentPage() != $tuyendung->lastPage())
                        <li class="page-item"><a class="page-link" href="{!! $tuyendung->url($tuyendung->currentPage() + 1) !!}">Next</a></li>
                        @endif
                      </ul>
                </div>
               <div class="col-md-4 col-lg-4">
                    <div class="control-bar">
                        <p class="p-title">Tin Mới Nhất</p>
                        <?php 
                            $tuyendung = DB::table('bds_tuyendung')->select('title')->orderBy('id','DESC')->take(6)->get();
                        ?>                
                        <ul class="u-title">
                            @foreach($tuyendung as $item)
                            <li class="pb-3 pr-2">
                                <a href="{{route('chi-tiet-tuyen-dung',[$td->alias])}}">{{$item->title}}</a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection