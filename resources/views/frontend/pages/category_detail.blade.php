 @extends('frontend.postings')
 @section('content')
    <!-- Product Detail-->
    <section class="product-detail">
        <div class="contanier">
            <div class="pro-content" id="matbang">
                
                 <h3>{!! $newspost->title !!}</h3>
                <p>{!! $newspost->content !!}</p>
            </div>

             @include('frontend.pages.contactUs')
        </div>
    </section>
@endsection