
<!DOCTYPE HTML>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Khôi phục mật khẩu</title>
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
 <style type="text/css" media="screen">
  body {
    background-color: #c8c6c6;
  }
   .form-gap {
    padding-top: 70px;
}
 </style>
</head>

 <div class="form-gap"></div>
<div class="container">
  <div class="row">
    <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="text-center">
                  <h3><i class="fa fa-lock fa-4x"></i></h3>
                  <h2 class="text-center">Cập nhật mật khẩu</h2>
                 
                  @if (\Session::has('success'))
                      <span class="alert-success"  id="flash-message" style="padding: 5px;color: #f7f7f7">
                          {!! __(\Session::get('success')) !!}
                      </span>
                  @endif
                  @if (\Session::has('error'))
                      <div class="alert-danger"  id="flash-message" style="padding: 5px;color: #f7f7f7" />
                          {!! __(\Session::get('error')) !!}
                      </div>
                  @endif
                  <div class="panel-body">                   
                   @if (session('status'))
                      <div class="alert alert-success" role="alert">
                          {{ session('status') }}
                      </div>
                   @endif
                    @if ($data['result'] == 1)    
                    <form id="register-form" action="{{ route('admin-password.change-password') }}" role="form" autocomplete="off" class="form" method="post">
                      @csrf
                      <div class="form-group">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="glyphicon glyphicon-wrench color-blue"></i></span>
                          <input name="password"  class="form-control"  type="password" placeholder="Mật khẩu mới">
                          @if ($errors->has('password'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('password') }}</strong>
                              </span>
                          @endif
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="glyphicon glyphicon-wrench color-blue"></i></span>
                          <input class="form-control" id="password2" name="password2"  type="password" placeholder="Xác nhận mật khẩu mới">
                          <input id="token" type="hidden" value="{!! $data['token'] !!}" class="form-control" name="token">
                         
                        </div>
                      </div>
                      <div class="form-group">
                        <input name="Cập nhật ngay" class="btn btn-lg btn-primary btn-block" value="Cập nhật ngay" type="submit">
                      </div>
                    </form>
                     @endif
                      @if ($data['result'] == 2)
                          <div class="alert alert-success">
                              {!! $data['result_message'] !!}
                          </div>
                      @endif
                  </div>
                </div>
              </div>
            </div>
          </div>
    </div>
</div>
</body>
</html>
