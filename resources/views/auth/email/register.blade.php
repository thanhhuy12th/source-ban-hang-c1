<!DOCTYPE html>
<html lang="en">

<head>
    @include('frontend.blocks.head')
</head>

<body>
    <!-- Mở đầu và menu -->
    @include('frontend.blocks.menu')
    <div class="wrap-login ">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">Succuss!</div>
                        <div class="card-body">
                            <div class="form-group ">                       
                                <div class="col-md-12">
                                     <div class="alert alert-success">  
                                        <strong>Succuss!</strong> Kích hoạt tài khoản thành công. <a href="{{route('dangnhap')}}" class="alert-link">Click vào đây để về trang đăng nhập</a> 
                                    </div>  
                                </div>
                            </div>           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer -->
    <footer class="footer">
        @include('frontend.blocks.footer')
    </footer>
</body>
</html>
