@extends('admin.master')
@section('title','Chỉnh Sửa Tin Đăng')
@section('content')
@include('admin.blocks.alert')
@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('tindang.index')}}" class="breadcrumb-item">Danh sách</a>
                <span class="breadcrumb-item active">Cập nhật</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
        	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
            <form class="form-validate-jquery" action="{{route('tindang.update',['id' => $post->id])}}" method="POST" enctype="multipart/form-data">
                @method('PUT')
            	@csrf
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">{{trans('template.Edit Postings')}}</legend>

                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                            <label class="lable_edit">Danh mục</label>
                            <select name="category_id" class="form-control">                               
                                    @for($i = 0; $i < count($cate_option); $i++)
                                        <option value="{!!$cate_option[$i]['id']!!}" {{ (old('category_id',$post->category_id) == $cate_option[$i]['id'])? 'selected': ''}}>{!!$cate_option[$i]['name']!!}</option>
                                    @endfor                                                     
                            </select>
                        </div>    
                        <div class="col-sm-6">
                            <label class="lable_edit">{{trans('template.Title Post')}}<span class="text-danger">*</span></label>
                            <input value="{{old('title',$post->title)}}" type="text" name="title" class="form-control" required="" >
                        </div>                                           
                    </div>
                    <div class="form-group col-sm-12 row">                       
                        <div class="col-sm-6">
                            <label class="lable_edit"></label>
                            <img src="{{asset('uploads/postings/'.$post->image)}}" width="15%">                            
                        </div>                                                              
                    </div>
                    <div class="form-group col-sm-12 row">                       
                        <div class="col-sm-6">
                            <label class="lable_edit">{{trans('template.Image Post')}}<span class="text-danger">*</span></label>
                            <input type="file" name="image" class="form-input-styled" >
                            <input type="hidden" name="imagehidden" value="{{$post->image}}" >
                        </div>
                        <div class="col-sm-6">
                            <label class="lable_edit">{{trans('template.Location')}}<span class="text-danger">*</span></label>
                            <input type="text" name="vitri" class="form-control" value="{{old('vitri',$post->vitri)}}" />
                        </div>                                           
                    </div>
                    <div class="form-group col-sm-12 row">                       
                        <div class="col-sm-6">
                            <label class="lable_edit">{{trans('template.Province')}}<span class="text-danger">*</span></label>
                            <select id="province" name="province" class="form-control">
                                {{-- <option value="-1">-- Chọn Tỉnh Thành --</option> --}}                         
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label class="lable_edit">{{trans('template.District')}}<span class="text-danger">*</span></label>
                            <select id="district" name="district" class="form-control">
                                {{-- <option value="-1">-- Chọn Quận Huyện --</option> --}}
                            </select>
                        </div>                                           
                    </div>
                     <div class="form-group col-sm-12 row">                       
                        <div class="col-sm-6">
                            <label class="lable_edit">{{trans('template.Status')}}<span class="text-danger">*</span></label>
                            <select name="status" class="form-control" />
                                <option value="0" {{($post->status ==0)? 'selected':''}}>Không nổi bật</option>
                                <option value="1" {{($post->status ==1)? 'selected':''}}>Nổi bật</option>                                
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label class="lable_edit">{{trans('template.Browse Articles')}}<span class="text-danger">*</span></label>
                            <select name="duyetbai" class="form-control" />
                                <option value="0" {{($post->duyetbai ==0)? 'selected':''}}>Không duyệt bài</option>
                                <option value="1" {{($post->duyetbai ==1)? 'selected':''}}>Duyệt bài</option> 
                            </select>
                        </div>                                           
                    </div>
                    <!-- Basic content input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-12">{{trans('template.Content')}}</label>
                    </div>
                    
                    <hr>
                    
                    <div class="card-body">
                        <ul class="nav nav-tabs">
                            <li class="nav-item"><a href="#basic-tab1" class="nav-link active show" data-toggle="tab">{{trans('template.Intro Post')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab2" class="nav-link" data-toggle="tab">{{trans('template.Acreage Content')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab3" class="nav-link" data-toggle="tab">{{trans('template.Price Content')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab4" class="nav-link" data-toggle="tab">{{trans('template.Overview')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab5" class="nav-link" data-toggle="tab">{{trans('template.Contact')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab6" class="nav-link" data-toggle="tab">{{trans('template.Location Post')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab7" class="nav-link" data-toggle="tab">{{trans('template.Image Post')}}</a></li>
                        </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade active show" id="basic-tab1">
                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name"></label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <textarea rows="6" name="intro" class="form-control col-md-12 col-xs-12" >{{old('intro',$post->intro)}}</textarea>
                            </div>
                        </div>

                       <div class="tab-pane fade" id="basic-tab2">
                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name"></label>
                              <div class="row">
                                <div class="col-lg-6">                                   
                                    <div class="input-group">
                                        <input type="text" name="dientichmatbang" class="form-control" value="{{old('dientichmatbang',$post->dientichmatbang)}}" placeholder="Diện tích mặt bằng" />
                                        <span class="input-group-append">
                                            <span class="input-group-text">m²</span>
                                        </span>
                                    </div>
                                </div>
                                 <div class="col-lg-6">                                   
                                    <div class="input-group">
                                        <input type="text" name="dientichsudung" class="form-control" value="{{old('dientichsudung',$post->dientichsudung)}}" placeholder="Diện tích sử dụng" />
                                        <span class="input-group-append">
                                            <span class="input-group-text">m²</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="tab-pane fade" id="basic-tab3">
                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name"></label>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">$</span>
                                        </span>
                                        <input type="text" name="price" class="form-control" value="{{old('price',$post->price)}}" placeholder="Giá bán" />
                                        <span class="input-group-append">
                                            <span class="input-group-text">.00</span>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select class="form-control form-control" name="typegia">
                                             <option value="1" {{($post->typegia ==1)? 'selected':''}}>VND</option>
                                             <option value="2" {{($post->typegia ==2)? 'selected':''}}>USD</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>                    
                  

                        <div class="tab-pane fade" id="basic-tab4">
                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name"></label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <textarea rows="10"  id="tongquanbreak" name="tongquan" class="form-control col-md-12 col-xs-12">{{old('tongquan',$post->tongquan)}}</textarea>                                
                            </div>
                             {{-- <p id="output2"></p> --}}
                        </div>

                        <div class="tab-pane fade" id="basic-tab5">
                             <label class="control-label col-md-12 col-sm-12 col-xs-12 mt-1" for="last-name"></label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                @php 
                                    $splittedstring=explode("||",$post->contact);   
                                @endphp
                               @for($i =0; $i<1; $i++)
                                 <div class="form-group row">
                                    <label class="col-form-label col-lg-2">Tên liên hệ:</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="name_contact" class="form-control" value="{{old('contact',$splittedstring[0])}}" placeholder="Nhập tên liên hệ" />
                                    </div>
                                </div>
                                  <div class="form-group row">
                                    <label class="col-form-label col-lg-2">Email:</label>
                                    <div class="col-lg-8">
                                        <input type="email" name="email_contact" class="form-control" value="{{old('contact',$splittedstring[1])}}" id="email" placeholder="Email" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-lg-2">Số điện thoại:</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="phone_contact" class="form-control" value="{{old('contact',$splittedstring[2])}}" placeholder="Số điện thoại" />
                                    </div>
                                </div>
                                 <div class="form-group row">
                                    <label class="col-form-label col-lg-2">Zalo:</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="zalo_contact" class="form-control" value="{{old('contact',$splittedstring[3])}}" placeholder="Zalo" />
                                    </div>
                                </div>
                                @endfor
                            </div>
                        </div>

                        <div class="tab-pane fade" id="basic-tab6">
                            <div class="container mt-3">
                                
                                  <!-- Trigger the modal with a button -->
                                  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" style="padding: 0px 20px; font-size: 14px; border-radius: 2rem;">Hướng dẫn nhúng iframe Google Maps</button>
                                  <!-- Modal -->
                                  <div class="modal fade" id="myModal" role="dialog">
                                    <div class="modal-dialog" style="top:110px">
                                    
                                      <!-- Modal content-->
                                      <div class="modal-content">
                                        <div class="modal-header">
                                         
                                          <h4 class="modal-title">Hướng dẫn nhúng iframe google map</h4>
                                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <h6>Bước 1: Đăng nhập và truy cập vào website Google Maps tại địa chỉ: <a href="https://www.google.com/maps/preview" target="_blank" />https://www.google.com/maps/preview</a></h6>
                                                <p> - Nhập địa chỉ của bạn tại ô tìm kiếm bản đồ.</p>
                                            <h6>Bước 2: Click vào menu để lấy mã code</h6>
                                                <p> - Click vào menu bản đồ Google Maps</p>
                                                <p> - Click vào mục Chia sẻ & Nhúng bản đồ – Share & Embed map để lấy mã code</p>
                                            <h6>Bước 3: Lấy mã code nhúng vào website</h6>
                                                <p> - Click vào mục Nhúng bản đồ – Tab Embel maps</p>
                                                <p> - Copy mã code Google Maps nhúng code vào website</p>
                                        </div>
                                        <div class="modal-footer">
                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                      </div>                                      
                                    </div>
                                  </div>                                  
                                </div>
                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name"></label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                               <textarea rows="6" name="vitridacdia" class="form-control col-md-12 col-xs-12">{{old('vitridacdia',$post->vitridacdia)}}</textarea>
                            </div>
                        </div>
                         <div class="tab-pane fade" id="basic-tab7">
                            <div class="col-lg-9 mt-3 row">
                                @php 
                                    $splittedstring=explode("||",$post->hinh);   
                                @endphp
                               @for($i =0; $i<count($splittedstring)-1; $i++)
                                    
                                    <img class="form-control" src="{{ asset('public/uploads/postings/'.$splittedstring[$i])}}" style="width:100px; height:100px">
                              @endfor              

                            </div>
                           
                          <div class="form-group row">                               
                               <div class="col-lg-10">                                   
                                    <input type="file" class="file-input-extensions"  name="hinh[]" multiple="multiple" data-fouc>
                                    <input type="hidden" name="hinhhidden" value="{{$post->hinh}}" >
                                    <span class="form-text text-muted">Ghi chú: Chỉ cho phép định dạng <code>jpg</code>, <code>jpeg</code>, <code>png</code>, <code>gif</code> and <code>svg</code> và  kích thước tối đa tệp tin là <code>2MB</code>.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /basic content input -->
                </fieldset>

                <div class="d-flex justify-content-center align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.Reset')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.Update')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
<script type="text/javascript">
    $(document).ready(function(){
        var url = "../../../api/province";
        $.getJSON(url,function(result){
            $.each(result, function(i, field){
                var id      = field.provinceid;
                var name    = field.name;
                var type    = field.type;
                 var selec   = '';
                    if (id == {{$post->province}}) {
                        selec ='selected';
                        $("#province").append('<option '+selec+' value="'+id+'">'+type+" "+name+'</option>');
                    }else{
                        $("#province").append('<option value="'+id+'">'+type+" "+name+'</option>');
                    }
            });
        });

        var idprovince = $("#province").val();
        var url = "../../../api/district/{{$post->province}}";
            $.getJSON(url,function(result){              
                $.each(result, function(i, field){
                    var id      = field.districtid;
                    var name    = field.name;
                    var type    = field.type;
                    var select   = '';
                    if (id == {{$post->district}}) {
                            select ='selected=""';
                            $("#district").append('<option '+select+' value="'+id+'">'+type+" "+name+'</option>');
                        }else{
                           $("#district").append('<option value="'+id+'">'+type+" "+name+'</option>');
                        }
                    
                });
            });

        //Khi chọn tỉnh thành
        $("#province").change(function() {
            $("#district").html('<option value="-1">----Chọn quận/huyện----</option>');
            var idprovince = $("#province").val();
            var url = "../../../api/district/"+idprovince;
            $.getJSON(url,function(result){
                console.log(result);
                $.each(result, function(i, field){
                    var id      = field.districtid;
                    var name    = field.name;
                    var type    = field.type;
                    $("#district").append('<option value="'+id+'">'+type+" "+name+'</option>');
                });
            });
        });
    })


  let textareaText = $('#tongquanbreak').val();
     textareaText = textareaText.replace(/\r?\n/g, '<br />');
        // $('#output2').html(textareaText);

</script>
@endsection