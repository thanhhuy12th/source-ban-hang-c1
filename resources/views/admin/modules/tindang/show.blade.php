@extends('admin.master') 
@section('title','Danh Sách Tin Đăng') 
@section('content')
@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('tindang.index')}}" class="breadcrumb-item">Danh sách</a>
                <span class="breadcrumb-item active">Show</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

<div class="content">

    <!-- Page length options -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">{{trans('template.List Postings')}}</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>
        <form action="{{route('tindang.destroy',['action' => 'id'])}}" name="listForm" method="POST">
            @method('DELETE')
            @csrf
        <table class="table datatable-show-all">
            <thead>
                <tr>
                    <th>
                        <input type="checkbox" name="chkall"  onClick="checkallClick(this);">
                    </th>
                    <th>ID</th>
                    <th>Tiêu đề</th>
                    <th>Nổi bật</th>
                    <th>Duyệt bài</th>
                    <th>Hoạt động</th>
                </tr>
            </thead>
            <tbody>
            	@foreach($postings as $item)
                <tr>
                    <td>
                        <input type="checkbox" name="chk[]" value="{{$item->id}}">
                    </td>
                    <td>{{$item->id}}</td>
                    <td>{{ $item->title }}</td>
                    <td>
                        <div class="form-check form-check form-check-switchery form-check-switchery-sm">
                            <label class="form-check-label">
                                <input type="checkbox" data-id="{{ $item->id }}" name="status" class="form-input-switchery" {{ $item->status ? 'checked' : '' }}  >
                            </label>
                        </div>
                    </td>
                    <td>
                        <div class="form-check form-check-switchery">
                            <label class="form-check-label">
                                <input type="checkbox" data-id="{{ $item->id }}" name="duyetbai" class="form-check-input-switchery" {{ $item->duyetbai ? 'checked' : '' }} >
                            </label>
                        </div>
                    </td>
                    <td>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($item ->created_at))->diffForHumans() }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
       <button type="submit" class="btn btn-danger" onclick="return acceptDelete()" style="margin-left: 10px" title="Xóa">
            <i class="icon-trash" aria-hidden="true"></i></button><br/><br>
        </form>
    </div>
    <!-- /page length options -->
</div>
<script type="text/javascript">
$(document).ready(function () {
    $('.form-check-input-switchery').on('click',function () {
        var Id = $(this).data('id');
        var duyetbai = this.checked;
        $.ajax({
            type: "POST",
            url: "../../api/duyet-bai",
            data: {
                id: Id,
                duyetbai: duyetbai
            },
            success: function (result) {  
                    console.log(result.msg);
            },
            error: function () {
                console.log('Lỗi rồi,debug đi nhé.');
            }
        })
    });
});
$(document).ready(function () {
    $('.form-input-switchery').on('click',function () {
        var Id = $(this).data('id');
        var status = this.checked;
        $.ajax({
            type: "POST",
            url: "../../api/tin-dang",
            data: {
                id: Id,
                status: status
            },
            success: function (result) {  
                    console.log(result.msg);
            },
            error: function () {
                console.log('Bị hack rồi nha bạn :D');
            }
        })
    });
});

</script>
@endsection