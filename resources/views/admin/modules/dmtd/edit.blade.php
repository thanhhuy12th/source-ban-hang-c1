@extends('admin.master')
@section('title','Sửa Danh Mục Tin Đăng')
@section('content')
@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('dmtd.index')}}" class="breadcrumb-item">Danh sách</a>
                <span class="breadcrumb-item active">Cập nhật</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="form-validate-jquery" action="{{route('dmtd.update',['id'=>$category->id])}}" method="POST" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">{{trans('template.Edit Category')}}</legend>

                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                            <label class="lable_edit">Danh mục</label>
                            <select name="parent_id" class="form-control">
                                <option value="0">ROOT</option>
                               {{-- @foreach($menus as $menu)
                                    <option value="{{$menu->id}}" {{ (old('parent_id',$menu->parent_id) == $menu->id)? 'selected': ''}}>{{$menu->name}}</option>
                               @endforeach --}}                
                            </select>
                        </div>    
                        <div class="col-sm-6">
                            <label class="lable_edit">{{trans('template.Name Category')}}<span class="text-danger">*</span></label>
                            <input value="{{old('name',$category->name)}}" type="text" name="name" class="form-control" required="" >
                        </div>                                           
                    </div>
                    <div class="form-group col-sm-12 row">                        
                        <div class="col-sm-6">
                            <label class="lable_edit">{{trans('template.Location Category')}}</label>
                            <input value="{{old('location',$category->location)}}" type="text" name="location" class="form-control"  >
                        </div>                                           
                    </div>
                </fieldset>

                <div class="d-flex justify-content-center align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.Reset')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.Update')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection