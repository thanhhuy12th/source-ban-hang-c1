@extends('admin.master')
@section('title','Liên Hệ Chúng Tôi')
@section('content')
@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('contactus.index')}}" class="breadcrumb-item">Danh sách</a>
                <span class="breadcrumb-item active">Cập nhật</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection
@include('admin.blocks.alert')
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
        	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
            <form class="form-validate-jquery" action="{{route('contactus.update',['action' => 'id'])}}" method="POST" enctype="multipart/form-data">
                @method('PUT')
            	@csrf
                <fieldset class="mb-3">
                    <center>
                        <legend class="text-uppercase font-size-lg font-weight-bold">{{trans('template.ContactUs')}}</legend>
                    </center>
                     <!-- Basic hotline1 input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Hotline')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                            <input type="text" name="HotLine1" class="form-control" value="{{$hotline1->content}}" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.PhoneName')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                            <input type="text" name="PhoneName1" class="form-control" value="{{$phonename1->phonename}}" placeholder="Your Name" />
                        </div>
                    </div>
                    <!-- /basic hotline1 input -->

                     <!-- Basic hotline2 input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Hotline')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                            <input type="text" name="HotLine2" class="form-control" value="{{$hotline2->content}}" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.PhoneName')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                            <input type="text" name="PhoneName2" class="form-control" value="{{$phonename2->phonename}}" placeholder="Your Name" />
                        </div>
                    </div>
                    <!-- /basic hotline2 input -->

                     <!-- Basic hotline3 input -->
                   <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Hotline')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                            <input type="text" name="HotLine3" class="form-control" value="{{$hotline3->content}}" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.PhoneName')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-3">
                            <input type="text" name="PhoneName3" class="form-control" value="{{$phonename3->phonename}}" placeholder="Your Name" />
                        </div>
                    </div>
                    <!-- /basic hotline3 input -->

                </fieldset>

                <div class="d-flex justify-content-end align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.Reset')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.Update')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection