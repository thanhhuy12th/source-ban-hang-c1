@extends('admin.master')
@section('title','Chỉnh Sửa Tin Tức')
@section('content')
@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('tintuc.index')}}" class="breadcrumb-item">Danh sách</a>
                <span class="breadcrumb-item active">Cập nhật</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection
@include('admin.blocks.alert')
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
        	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
            <form class="form-validate-jquery" action="{{route('tintuc.update',['id' => $news->id])}}" method="POST" enctype="multipart/form-data">
                @method('PUT')
            	@csrf
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">{{trans('template.News')}}</legend>
                        <div class="form-group col-sm-12 row">
                            <div class="col-sm-6">
                                <label class="lable_edit">Danh mục</label>
                                    <select name="parent_id" class="form-control">                                   
                                        @for($i = 0; $i < count($news_option); $i++)
                                            <option value="{!!$news_option[$i]['id']!!}" {{ (old('parent_id',$news->parent_id) == $news_option[$i]['name'])? 'selected': ''}}>{!!$news_option[$i]['name']!!}</option>
                                       @endfor                           
                                    </select>
                            </div>    
                            <div class="col-sm-6">
                                <label class="lable_edit">{{trans('template.Title News')}}<span class="text-danger">*</span></label>
                                <input value="{{old('title',$news->title)}}" type="text" name="title" class="form-control" required="" >
                            </div>                                           
                        </div>
                        <div class="form-group col-sm-12 row">                            
                            <div class="col-sm-6">
                                <label class="lable_edit"></label>
                                <img src="{{asset('uploads/postings/'.$news->image)}}" width="30%">
                            </div>                                           
                        </div>
                        <div class="form-group col-sm-12 row">                            
                            <div class="col-sm-6">
                                <label class="lable_edit">{{trans('template.Image News')}}<span class="text-danger">*</span></label>
                                <input type="file" name="image" class="form-input-styled" >
                                <input type="hidden" name="imagehidden" value="{{$news->image}}" >
                            </div>                                           
                        </div>
                    <!-- Basic content input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-12">{{trans('template.Content')}}</label>
                    </div>
                    
                    <hr>
                    <div class="card-body">
                        <ul class="nav nav-tabs">
                            <li class="nav-item"><a href="#basic-tab1" class="nav-link active show" data-toggle="tab">{{trans('template.Intro News')}}<span class="text-danger">*</span></a></li>
                            <li class="nav-item"><a href="#basic-tab2" class="nav-link" data-toggle="tab">{{trans('template.Content News')}}<span class="text-danger">*</span></a></li>
                        </ul>

                    <div class="tab-content">
                        <div class="tab-pane fade active show" id="basic-tab1">
                          
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <textarea rows="6" name="intro" class="form-control col-md-12 col-xs-12" >{{old('intro',$news->intro)}}</textarea>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="basic-tab2">
                          
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <textarea rows="6" id="editor1" name="content" class="form-control col-md-12 col-xs-12">{{old('content',$news->content)}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /basic content input -->
                </fieldset>

                <div class="d-flex justify-content-center align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.Reset')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.Update')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection