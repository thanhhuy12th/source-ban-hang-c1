@extends('admin.master') 
@section('title','Danh Sách Tin Tức') 
@section('content')
@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('tintuc.index')}}" class="breadcrumb-item">Danh sách</a>
                {{-- <span class="breadcrumb-item active"></span> --}}
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection
@include('admin.blocks.alert')
<div class="content">

    <!-- Page length options -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">{{trans('template.List News')}}</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <a href="{{route('tintuc.create')}}">
                <button type="button" class="btn btn-primary">Thêm Tin Tức</button>
            </a>
        </div>
        <form action="{{route('tintuc.destroy',['action' => 'delete'])}}" name="listForm" method="POST">
            @method('DELETE')
            @csrf
        <table class="table datatable-show-all">
            <thead>
                <tr>
                    <th>
                        <input type="checkbox" name="chkall"  onClick="checkallClick(this);">
                    </th>
                    <th>#</th>
                    <th>Tiêu đề</th>
                    <th>Mô Tả</th>
                    <th>Hoạt Động</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
            	@foreach($news as $item)
                <tr>
                    <td>
                        <input type="checkbox" name="chk[]" value="{{$item->id}}">
                    </td>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->title }}</td>
                    <td>{{ $item->intro }}</td>
                    <td>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($item ->created_at))->diffForHumans() }}</td>
                    <td class="text-center">                        
                        <a href="{{ route('tintuc.edit',['tintuc' => $item->id]) }}" class="list-icons-item text-primary-600" title="Sửa"><i class="icon-pencil7"></i></a> &nbsp;                      
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
            <button type="submit" class="btn btn-danger" onclick="return acceptDelete()" style="margin-left: 10px" title="Xóa">
            <i class="icon-trash" aria-hidden="true"></i></button><br/><br>              
        </form>
    </div>
    <!-- /page length options -->
</div>
@endsection