@extends('admin.master')
@section('title','Giới Thiệu')
@section('content')
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
        	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
            <form class="form-validate-jquery" action="{{route('gioithieu.update',['id' => $introduces->id])}}" method="POST" enctype="multipart/form-data">
                @method('PUT')
            	@csrf
                <fieldset class="mb-3">
                    <center>
                        <legend class="text-uppercase font-size-sm font-weight-bold">{{trans('template.Introduce')}}</legend>
                    </center>

                       <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Name Introduce')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="name" class="form-control" value="{{$introduces->name}}" />
                        </div>
                    </div>
                    <!-- /basic text input -->

                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Content Introduce')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <textarea rows="6" id="editor1" name="content" class="form-control col-md-12 col-xs-12">{{$introduces->content}}</textarea>
                        </div>
                    </div>
                    <!-- /basic text input -->

                </fieldset>

                <div class="d-flex justify-content-end align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.Reset')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.Update')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection