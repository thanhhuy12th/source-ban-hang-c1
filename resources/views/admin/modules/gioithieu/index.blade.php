@extends('admin.master')
@section('title','Giới Thiệu')
@section('content')
@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('gioithieu.index')}}" class="breadcrumb-item">Danh sách</a>
                <span class="breadcrumb-item active">Cập nhật</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection
@include('admin.blocks.alert')

<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="form-validate-jquery" action="{{route('gioithieu.update',['action' => 'id'])}}" method="POST" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <fieldset class="mb-3">
                    <center>
                        <legend class="text-uppercase font-size-sm font-weight-bold">{{trans('template.Introduce')}}</legend>
                    </center>

                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Content Introduce')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <textarea rows="6" id="editor1" name="GioiThieu" class="form-control col-md-12 col-xs-12">{{$introduces->content}}</textarea>
                        </div>
                    </div>
                    <!-- /basic text input -->

                </fieldset>

                <div class="d-flex justify-content-end align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.Reset')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.Update')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection