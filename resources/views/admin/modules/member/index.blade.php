@extends('admin.master') 
@section('title','Danh Sách Thành Viên') 
@section('content')
@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('member.index')}}" class="breadcrumb-item">Danh sách</a>
                {{-- <span class="breadcrumb-item active"></span> --}}
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection
@include('admin.blocks.alert')

<div class="content">

    <!-- Page length options -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">{{trans('template.List Member')}}</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>
        <form action="{{route('member.destroy',['action' => 'delete'])}}" name="listForm" method="POST">
            @method('DELETE')
            @csrf
        <table class="table datatable-show-all">
            <thead>
                <tr>
                    <th class="col-md-2" style="width: auto;">
                        <input type="checkbox" name="chkall"  onClick="checkallClick(this);">
                    </th>
                    <th class="col-md-2" style="width: auto;">Name</th>
                    <th class="col-md-2" style="width: auto;">Email</th>
                    <th class="col-md-2" style="width: auto;">Phone</th>
                    <th class="col-md-2" style="width: auto;">Address</th>
                    <th class="col-md-2" style="width: auto;">Hoạt động</th>
                    <th class="text-center col-md-2" style="width: auto;">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($member as $item)
                <tr>
                    <td>
                        <input type="checkbox" name="chk[]" value="{{$item->id}}">
                    </td>
                    <td>{{$item->username}}</td>
                    <td>{{$item->email}}</td>
                    <td>{{$item->phone}}</td>
                    <td>{{$item->address}}</td>
                  {{--   <td>{{ date("d/m/Y-H:i:s",strtotime($item ->created_at))}}</td> --}}
                    <td>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($item ->created_at))->diffForHumans() }}</td>
                    <td class="text-center">
                        <a href="{{ route('member.edit',['member' => $item->id]) }}" class="list-icons-item text-primary-600" title="Sửa"><i class="icon-pencil7"></i></a> &nbsp;
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
            <button type="submit" class="btn btn-danger" onclick="return acceptDelete()" style="margin-left: 10px" title="Xóa">
            <i class="icon-trash" aria-hidden="true"></i></button><br/><br>              
        </form> 
    </div>
    <!-- /page length options -->
</div>
@endsection