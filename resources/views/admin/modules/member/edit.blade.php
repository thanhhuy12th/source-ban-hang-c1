@extends('admin.master')
@section('title','Edit Member')
@section('content')
@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('member.index')}}" class="breadcrumb-item">Danh sách</a>
                <span class="breadcrumb-item active">Cập nhật</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
        	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
            <form class="form-validate-jquery" action="{{route('member.update',['id' => $member->id])}}" method="POST" enctype="multipart/form-data">
                @method('PUT')
            	@csrf
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">{{trans('template.Edit Member')}}</legend>
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                            <label class="lable_edit">{{trans('template.Fullname Member')}}<span class="text-danger">*</span></label>
                            <input value="{{old('fullname',$member->fullname)}}" type="text" name="fullname" class="form-control">
                        </div>
                        <div class="col-sm-6">
                            <label class="lable_edit">{{trans('template.Username Member')}} <span class="text-danger">*</span></label>
                            <input value="{{old('username',$member->username)}}" type="text" name="username" class="form-control" >
                        </div>                        
                    </div>
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                            <label class="lable_edit">{{trans('template.Email Member')}}<span class="text-danger">*</span></label>
                            <input value="{{old('email',$member->email)}}" type="email" id="email" name="email" class="form-control" readonly="" >
                        </div>
                        <div class="col-sm-6">
                            <label class="lable_edit">{{trans('template.Phone Member')}}<span class="text-danger">*</span></label>
                            <input value="{{old('phone',$member->phone)}}" type="text" name="phone" class="form-control" >
                        </div>                        
                    </div>
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                            <label class="lable_edit">{{trans('template.Password')}}<span class="text-danger">*</span></label>
                           <input type="password" name="password" id="password" class="form-control" />
                        </div>
                        <div class="col-sm-6">
                            <label class="lable_edit">{{trans('template.Address')}}<span class="text-danger">*</span></label>
                            <input value="{{old('address',$member->address)}}" type="text" name="address" class="form-control" >
                        </div>                        
                    </div>
                     <div class="col-sm-6">
                            <label class="col-form-label col-lg-3"></label>
                            <img src="{{ asset('uploads/avatar/'.$member->image)}}" width="100px">
                    </div>
                    <!-- Styled file uploader -->
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                            <label class="lable_edit">{{trans('template.Image Member')}}</label>
                            <input type="file" name="image" class="form-input-styled" />
                        </div>
                    </div>
                    <!-- /styled file uploader -->
                </fieldset>

                <div class="d-flex justify-content-center align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.Reset')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.Update')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection