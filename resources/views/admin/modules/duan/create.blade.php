@extends('admin.master')
@section('title','Thêm Dự Án')
@section('content')
@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('duan.index')}}" class="breadcrumb-item">Danh sách</a>
                <span class="breadcrumb-item active">Thêm</span>
            </div>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
        	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
            <form class="form-validate-jquery" action="{{route('duan.store')}}" method="POST" enctype="multipart/form-data">
            	@csrf
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">{{trans('template.Add Project')}}</legend>

                    <div class="form-group col-sm-12 row">                       
                        <div class="col-sm-6">
                            <label class="lable_edit">{{trans('template.Name Project')}}<span class="text-danger">*</span></label>
                            <input type="text" name="name" class="form-control" value="{{old('name')}}" required="" />
                        </div>
                        <div class="col-sm-6">
                            <label class="lable_edit">{{trans('template.Title Project')}}<span class="text-danger">*</span></label>
                            <input type="text" name="title" class="form-control" value="{{old('title')}}" required="" />
                        </div>                                           
                    </div>
                    <div class="form-group col-sm-12 row">                       
                        <div class="col-sm-6">
                            <label class="lable_edit">Ảnh đại diện<span class="text-danger">*</span></label>
                            <input type="file" name="image" class="form-input-styled" required="">
                        </div>
                        <div class="col-sm-6">
                            <label class="lable_edit">Trạng thái<span class="text-danger">*</span></label>
                            <select name="status" class="form-control" />
                                <option value="0">Không nổi bật</option>
                                <option value="1">Nổi bật</option>                                
                           </select>
                        </div>                                           
                    </div>
                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-12">{{trans('template.Content')}}</label>
                    </div>
                    <!-- /basic text input -->
                    <hr>
                    <div class="card-body">
                        <ul class="nav nav-tabs">
                            <li class="nav-item"><a href="#basic-tab1" class="nav-link active show" data-toggle="tab">{{trans('template.Intro Post')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab2" class="nav-link" data-toggle="tab">{{trans('template.Acreage Content')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab3" class="nav-link" data-toggle="tab">{{trans('template.Price Content')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab4" class="nav-link" data-toggle="tab">{{trans('template.Overview')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab5" class="nav-link" data-toggle="tab">{{trans('template.Location Post')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab6" class="nav-link" data-toggle="tab">{{trans('template.Flat')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab7" class="nav-link" data-toggle="tab">{{trans('template.Image Post')}}</a></li>
                            <li class="nav-item"><a href="#basic-tab8" class="nav-link" data-toggle="tab">{{trans('template.Contact')}}</a></li>
                        </ul>

                  <div class="tab-content">
                        <div class="tab-pane fade active show" id="basic-tab1">                          
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <textarea rows="1" name="intro" class="form-control col-md-12 col-xs-12" >{{old('intro')}}</textarea>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="basic-tab2">
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" name="dt" class="form-control" value="{{old('dt')}}" placeholder="Diện tích " />
                                    <span class="input-group-append">
                                        <span class="input-group-text">m²</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                         <div class="tab-pane fade" id="basic-tab3">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">$</span>
                                        </span>
                                        <input type="text" name="giaban" class="form-control" value="{{old('giaban')}}" placeholder="Giá bán" />
                                        <span class="input-group-append">
                                            <span class="input-group-text">.00</span>
                                        </span>
                                    </div>
                                </div> 
                            </div>                            
                        </div>
                        <div class="tab-pane fade" id="basic-tab4">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <textarea rows="6" id="editor1" name="tongquan" class="form-control col-md-12 col-xs-12">{{old('tongquan')}}</textarea>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="basic-tab5">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                               <textarea rows="6" id="vitridacdia" name="vitridacdia" class="form-control col-md-12 col-xs-12">{{old('vitridacdia')}}</textarea>
                            </div>
                        </div>
                         <div class="tab-pane fade" id="basic-tab6">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <textarea rows="6" id="matbang" name="matbang" class="form-control col-md-12 col-xs-12">{{old('matbang')}}</textarea>
                            </div>
                        </div>
                         <div class="tab-pane fade" id="basic-tab7">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                               <textarea rows="6" id="hinh" name="hinh" class="form-control col-md-12 col-xs-12">{{old('hinh')}}</textarea>
                            </div>
                        </div>            
                        <div class="tab-pane fade" id="basic-tab8">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <textarea rows="6" id="lienhe" name="contact" class="form-control col-md-12 col-xs-12">{{old('contact')}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                   
                </fieldset>

                <div class="d-flex justify-content-center align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.Reset')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.Submit')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection