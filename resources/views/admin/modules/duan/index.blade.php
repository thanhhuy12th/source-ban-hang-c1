@extends('admin.master') 
@section('title','Danh Sách Danh Mục Tin Tức') 
@section('content')
@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('duan.index')}}" class="breadcrumb-item">Danh sách</a>
                {{-- <span class="breadcrumb-item active"></span> --}}
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection
@include('admin.blocks.alert')

<div class="content">

    <!-- Page length options -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">{{trans('template.List Project')}}</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <a href="{{route('duan.create')}}">
                <button type="button" class="btn btn-primary">Thêm Dự Án</button>
            </a>
        </div>
        <form action="{{route('duan.destroy',['action' => 'delete'])}}" name="listForm" method="POST">
            @method('DELETE')
            @csrf
        <table class="table datatable-show-all">
            <thead>
                <tr>
                    <th>
                        <input type="checkbox" name="chkall"  onClick="checkallClick(this);">
                    </th>
                    <th>Tên dự án</th>
                    <th>Tiêu đề</th>
                    <th>Mô tả</th>
                    <th>Nổi bật</th>
                    <th>Hoạt động</th>
                    <th class="text-center">Chọn</th>
                </tr>
            </thead>
            <tbody>
            	@foreach($duan as $item)
                <tr>
                    <td>
                        <input type="checkbox" name="chk[]" value="{{$item->id}}">
                    </td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->title}}</td>
                    <td>{{$item->intro}}</td>
                    <td>
                       <div class="form-check form-check form-check-switchery form-check-switchery-sm">
                            <label class="form-check-label">
                                <input type="checkbox" data-id="{{ $item->id }}" name="status" class="form-input-switchery" value="1" {{ $item->status ? 'checked' : '' }}  >
                            </label>
                        </div>
                    </td>
                    <td>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($item ->created_at))->diffForHumans() }}</td>
                    <td class="text-center">                      
                        <a href="{{ route('duan.edit',['duan' => $item->id]) }}" class="list-icons-item text-primary-600" title="Sửa"><i class="icon-pencil7"></i></a> &nbsp;
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
             <button type="submit" class="btn btn-danger" onclick="return acceptDelete()" style="margin-left: 10px" title="Xóa">
            <i class="icon-trash" aria-hidden="true"></i></button><br/><br>
    </div>
    <!-- /page length options -->
</div>
<script type="text/javascript">

$(document).ready(function () {
    $('.form-input-switchery').on('click',function () {
        var Id = $(this).data('id');
        var status = this.checked;

        $.ajax({
            type: "POST",
            url: "../api/du-an",
            data: {
                id: Id,
                status: status
            },
            success: function (result) {  
                    console.log(result.msg);
            },
            error: function () {
                console.log('Bị hack rồi nha bạn :D');
            }
        })
    });
});

</script>
@endsection