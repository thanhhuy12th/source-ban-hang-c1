@extends('admin.master')
@section('title','Sửa Danh Mục Tin Tức')
@section('content')
@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('dmtt.index')}}" class="breadcrumb-item">Danh sách</a>
                <span class="breadcrumb-item active">Cập nhật</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
        	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
            <form class="form-validate-jquery" action="{{route('dmtt.update',['id'=> $cate->id])}}" method="POST" enctype="multipart/form-data">
                @method('PUT')
            	@csrf
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">{{trans('template.Add CateNews')}}</legend>

                    <div class="form-group col-sm-12 row"> 
                        <div class="col-sm-6">
                            <label class="lable_edit">{{trans('template.Name CateNews')}}<span class="text-danger">*</span></label>
                            <input value="{{old('name',$cate->name)}}" type="text" name="name" class="form-control" required="" >
                        </div>                                                                   
                        <div class="col-sm-6">
                            <label class="lable_edit">{{trans('template.Parent CateNews')}}<span class="text-danger">*</span></label>
                            <select name="parent_id" class="form-control">
                               <option value="0">ROOT</option>                               
                           </select>
                        </div>    
                    </div>
                   
                </fieldset>

                <div class="d-flex justify-content-center align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.Reset')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.Update')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection