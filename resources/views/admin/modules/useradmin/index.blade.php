@extends('admin.master') 
@section('title','Danh Sách UserAdmin') 
@section('content')
@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('useradmin.index')}}" class="breadcrumb-item">Danh sách</a>
                {{-- <span class="breadcrumb-item active">Current</span> --}}
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection
@include('admin.blocks.alert')

<div class="content">

    <!-- Page length options -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">{{trans('template.List Admin')}}</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <a href="{{route('useradmin.create')}}">
                <button type="button" class="btn btn-primary" class="">Thêm quản trị viên</button>
            </a>
        </div>
       {{--  <form action="" method="POST" name="listForm"> --}}
           
        <table class="table datatable-show-all">
            <thead>
                <tr>
                    <th>#</th>
                    <th>UserName</th>
                    <th>FullName</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Quyền</th>
                    <th>Hoạt động</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
            	@foreach($admin as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->username}}</td>
                    <td>{{$item->fullname}}</td>
                    <td>{{$item->email}}</td>
                    <td>{{$item->phone}}</td>
                    <td>
                    	@if ($item->role == 1)
    						<span class="badge badge-danger">Admin</span>
						@elseif ($item->role == 2)
    						<span class="badge badge-success">Admin duyệt bài</span>
						@else
    						<span class="badge badge-primary">Admin đăng bài</span>
						@endif
					</td>
                    <td>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($item ->created_at))->diffForHumans() }}</td>
                    <td class="text-center">
                        <a href="{{ route('useradmin.edit',['useradmin' => $item->id]) }}" class="list-icons-item text-primary-600" title="Sửa"><i class="icon-pencil7"></i></a> &nbsp;
                         <a href="{{ route('useradmin.destroy',['useradmin' => $item->id]) }}" onclick="return acceptDelete()" class="list-icons-item text-danger-600" title="Xóa"><i class="icon-trash"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>                   
    {{--  </form>  --}}
    </div>
    <!-- /page length options -->
</div>
@endsection