@extends('admin.master')
@section('title','Chỉnh Sửa Admin')
@section('content')
@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('useradmin.index')}}" class="breadcrumb-item">Danh sách</a>
                <span class="breadcrumb-item active">Cập nhật</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
        	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
            <form class="form-validate-jquery" action="{{route('useradmin.update',['id' => $admin->id])}}" method="POST" enctype="multipart/form-data">
            	@csrf
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">{{trans('template.Edit Admin')}}</legend>
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                            <label class="lable_edit">{{trans('template.Fullname Admin')}}<span class="text-danger">*</span></label>
                            <input value="{{old('fullname',$admin->fullname)}}" type="text" name="fullname" class="form-control" readonly="" >
                        </div>
                        <div class="col-sm-6">
                            <label class="lable_edit">{{trans('template.Username Admin')}} <span class="text-danger">*</span></label>
                            <input value="{{old('username',$admin->username)}}" type="text" name="username" class="form-control" readonly="">
                        </div>                        
                    </div>
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                            <label class="lable_edit">{{trans('template.Email Admin')}}<span class="text-danger">*</span></label>
                            <input value="{{old('email',$admin->email)}}" type="email" id="email" name="email" class="form-control" readonly="" >
                        </div>
                        <div class="col-sm-6">
                            <label class="lable_edit">{{trans('template.Phone Admin')}}<span class="text-danger">*</span></label>
                            <input value="{{old('phone',$admin->phone)}}" type="text" name="phone" class="form-control" >
                        </div>                        
                    </div>
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                            <label class="lable_edit">{{trans('template.Password')}}<span class="text-danger">*</span></label>
                            <input value="{{old('password')}}" type="password" name="password" class="form-control">
                        </div>
                        <div class="col-sm-6">
                            <label class="lable_edit">{{trans('template.Level Admin')}}<span class="text-danger">*</span></label>
                            <select name="role" class="form-control pro-edt-select form-control-primary">
                                <option value="1" {{($admin->role ==1)? 'selected':''}}>Admin</option>
                                <option value="2" {{($admin->role ==2)? 'selected':''}}>Admin duyệt bài</option>
                                <option value="3" {{($admin->role ==3)? 'selected':''}}>Admin đăng bài</option>                 
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                            <label class="col-form-label col-lg-3"></label>
                            <img src="{{ asset('uploads/avatar/'.$admin->image)}}" width="100px">
                    </div>
                    <!-- Styled file uploader -->
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                            <label class="lable_edit">{{trans('template.Image Admin')}}</label>
                            <input type="file" name="image" class="form-input-styled" />
                        </div>
                    </div>
                    <!-- /styled file uploader -->
                </fieldset>

                <div class="d-flex justify-content-center align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.Reset')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.Update')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection