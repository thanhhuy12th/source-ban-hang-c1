@extends('admin.master')
@section('title','Change Password')
@section('content')
@include('admin.blocks.alert')
@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('useradmin.index')}}" class="breadcrumb-item">Danh sách</a>
                <span class="breadcrumb-item active">Đổi mật khẩu</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="form-validate-jquery" action="" method="POST" enctype="multipart/form-data">
                @csrf
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">ĐỎI MẬT KHẨU</legend>

                     <!-- Password field -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Mật khẩu cũ<span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="password" name="currentpassword" id="password" class="form-control" placeholder="Nhập mật khẩu cũ" />
                        </div>
                    </div>
                    <!-- /password field -->

                    <!-- Password field -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Mật khẩu mới<span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="password" name="newpassword" id="password" class="form-control" placeholder="Cho phép tối thiểu 6 ký tự"/>
                        </div>
                    </div>
                    <!-- /password field -->

                    <!-- Repeat password -->
                    <div class="form-group row">
                       <label class="col-form-label col-lg-3">Nhập lại mật khẩu mới<span class="text-danger">*</span></label>
                       <div class="col-lg-4">
                           <input type="password" name="newpassword_confirmation" class="form-control" placeholder="Nhập lại mật khẩu mới"/>
                       </div>
                   </div>
                    <!-- /repeat password -->

                </fieldset>

                <div class="d-flex justify-content-center align-items-center" style="margin-right: 260px">
                    <button type="submit" class="btn btn-primary ml-4">Đổi mật khẩu<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection