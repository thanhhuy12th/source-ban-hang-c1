{{-- @extends('admin.master')
@section('title','Thêm Liên hệ')
@section('content')
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
        	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
            <form class="form-validate-jquery" action="{{route('lienhe.store')}}" method="POST" enctype="multipart/form-data">
            	@csrf
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">{{trans('template.Add Contact')}}</legend>

                    <div class="form-group col-sm-12 row"> 
                        <div class="col-sm-6">
                            <label class="lable_edit">{{trans('template.Name Contact')}}<span class="text-danger">*</span></label>
                            <input value="{{old('name')}}" type="text" name="name" class="form-control" required="" >
                        </div>
                        <div class="col-sm-6">
                            <label class="lable_edit">{{trans('template.Email Contact')}}<span class="text-danger">*</span></label>
                            <input type="email" name="email" class="form-control" value="{{old('email')}}" id="email" />
                        </div>                                           
                    </div>
                    <div class="form-group col-sm-12 row"> 
                        <div class="col-sm-6">
                            <label class="lable_edit">{{trans('template.Phone Contact')}}<span class="text-danger">*</span></label>
                            <input type="number" name="phone" class="form-control" value="{{old('phone')}}" />
                        </div>
                        <div class="col-sm-6">
                            <label class="lable_edit">{{trans('template.Address')}}<span class="text-danger">*</span></label>
                            <input type="text" name="address" class="form-control" value="{{old('address')}}" />
                        </div>                                           
                    </div>
    
                     <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">{{trans('template.Content Contact')}}<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <textarea rows="6" name="content" class="form-control col-md-12 col-xs-12"></textarea>
                        </div>
                    </div>
                    <!-- /basic text input -->

                </fieldset>

                <div class="d-flex justify-content-center align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.Reset')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.Submit')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection --}}