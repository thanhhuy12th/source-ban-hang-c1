@extends('admin.master')
@section('title','Cấu Hình Khác') 
@section('content')
<div class="content">
    <!-- Form validation -->
    <div class="card">
        <div class="card-body">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form class="form-validate-jquery" action="{{route('cauhinhkhac.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <fieldset class="mb-3">
                    <center>
                        <legend class="text-uppercase font-size-lg font-weight-bold">{{trans('template.Other Configuration')}}</legend>
                    </center>

                    <div class="col-md-12">

                        <div class="card-body">
                            <ul class="nav nav-tabs nav-tabs-solid border-0">
                                <li class="nav-item"><a href="#solid-tab1" class="nav-link active show" data-toggle="tab">{{trans('template.Generality')}}</a></li>
                                <li class="nav-item"><a href="#solid-tab2" class="nav-link" data-toggle="tab">{{trans('template.Customer')}}</a></li>
                                <li class="nav-item"><a href="#solid-tab3" class="nav-link" data-toggle="tab">{{trans('template.Email')}}</a></li>
                                <li class="nav-item"><a href="#solid-tab4" class="nav-link" data-toggle="tab">{{trans('template.Script')}}</a></li>
                            </ul>

                            <div class="tab-content">
                                 <!-- Basic hotline input -->
                                <div class="tab-pane fade active show" id="solid-tab1">
                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name">{{trans('template.Hotline')}}
                                    </label>
                                    <!-- <div class="col-md-12 col-sm-12 col-xs-12" hidden>
                                        <input type="text" name="name" value="Hotline" class="form-control" />
                                    </div> -->
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="text" name="Hotline" class="form-control" />
                                    </div>
                                    <!-- /Basic hotline input -->
                                     <br>
                                     <!-- Basic email input -->
                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name">{{trans('template.Email')}}
                                    </label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="text" name="Email" class="form-control" />
                                    </div>
                                    <!-- /Basic email input -->
                                    <br>
                                    <!-- Basic tu khoa input -->
                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name">{{trans('template.Keyword')}}</label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="text" name="SEO" class="form-control" />
                                    </div>
                                    <!-- /Basic tu khoa input -->
                                     <br>
                                     <!-- Basic mo ta input -->
                                     <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name">{{trans('template.Description')}}</label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="text" name="Description" class="form-control" />
                                    </div>
                                     <!-- /Basic mo ta input -->
                                     <br>
                                     <!-- Basic tieu de input -->
                                     <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name">{{trans('template.Title')}}</label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="text" name="Title" class="form-control" />
                                    </div>
                                    <!-- /Basic tieu de input -->
                                    <br>
                                    <!-- Basic link input -->
                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name">{{trans('template.Base url')}}</label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="text" name="BaseUrl" class="form-control" />
                                    </div>
                                    <!-- /Basic link input -->
                                    <br>
                                     <!-- Basic dia chi input -->
                                     <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name">{{trans('template.Address')}}</label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <textarea rows="4" name="Address" class="form-control col-md-12 col-xs-12"></textarea>
                                    </div>
                                     <!-- /Basic dia chi input -->
                                </div>

                                <!-- Basic khach hang VIP input -->
                                <div class="tab-pane fade" id="solid-tab2">
                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name">{{trans('template.Customer Vip Money')}}
                                    </label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="text" name="CustomerVip" class="form-control" />
                                    </div>
                                </div>
                                <!-- /Basic khach hang VIP input -->

                                <!-- Basic username input -->
                                <div class="tab-pane fade" id="solid-tab3">
                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name">{{trans('template.Username Gmail')}}
                                    </label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="text" name="Username" class="form-control" />
                                    </div>
                                <br>
                                <!-- /Basic username input -->

                                <!-- Basic password input -->
                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name">{{trans('template.Password Gmail')}}
                                    </label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="text" name="Password" class="form-control" />
                                    </div>
                                </div>
                                <!-- /Basic password input -->

                                <!-- Basic Chatbox input -->
                                <div class="tab-pane fade" id="solid-tab4">
                                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="last-name">{{trans('template.Chatbox')}}</label>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <textarea rows="4" name="Chatbox" class="form-control col-md-12 col-xs-12"></textarea>
                                    </div>
                                </div>
                                <!-- /Basic Chatbox input -->
                            </div>
                        </div>

                    </div>

                </fieldset>

                <div class="d-flex justify-content-end align-items-center">
                    <button type="reset" class="btn btn-light" id="reset">{{trans('template.Reset')}}<i class="icon-reload-alt ml-2"></i></button>
                    <button type="submit" class="btn btn-primary ml-3">{{trans('template.Submit')}}<i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

</div>
@endsection