<div class="navbar navbar-expand-md navbar-dark">
    <style>
        .height-logo-icon{
            height: 50px !important;
        }
        .height-logo-icon img{
            height: 25px !important;
            width: 125px !important;
        }
    </style>
    <div class="navbar-brand height-logo-icon">
        <a href="./" class="d-inline-block">
            {{-- <img src="{{asset('backend/global_assets/images/logo_light.png')}}" alt=""> --}}
            <img src="{{ asset('uploads/logo/'.$logo ->content)}}" width="100%">
           
        </a>
    </div>

    <div class="d-md-none">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
            <i class="icon-tree5"></i>
        </button>
        <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
            <i class="icon-paragraph-justify3"></i>
        </button>
    </div>

    <div class="collapse navbar-collapse" id="navbar-mobile">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                    <i class="icon-paragraph-justify3"></i>
                </a>
            </li>     
        </ul>
        <span class="badge bg-success ml-md-3 mr-md-auto">Online</span>
        <ul class="navbar-nav">
            <li class="nav-item dropdown">
                <a href="#" class="navbar-nav-link dropdown-toggle caret-0" data-toggle="dropdown">
                    <i class="icon-people"></i>
                    <span class="d-md-none ml-2">Users</span>
                    <span class="badge badge-pill bg-warning-400 ml-auto ml-md-0">{{$count}}</span>
                </a>              
            </li>
            <li class="nav-item dropdown dropdown-user">
                <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
                    {{-- <img src="{{ asset('backend/global_assets/images/image.png') }}" class="rounded-circle mr-2" height="34" alt=""> --}}
                    <img src="{{ asset('uploads/avatar/'.Auth::guard('admin')->user()->image) }}" class="rounded-circle mr-2" height="34" alt="">
                    <span>{{Auth::guard('admin')->user()->fullname}}</span>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                   
                    <a href="{{ route('useradmin.edit',[Auth::guard('admin')->user()->id]) }}" class="dropdown-item"><i class="icon-cog5"></i> Thông tin cá nhân</a>
                    <a href="{{ route('change-password')}}" class="dropdown-item"><i class="icon-key"></i>Đổi mật khẩu</a>
                    <a href="{{route('getLogout')}}" class="dropdown-item"><i class="icon-switch2"></i>Đăng xuất</a>
                </div>
            </li>
        </ul>
    </div>
</div>