<div class="navbar navbar-expand-lg navbar-light">
    <div class="text-center d-lg-none w-100">
        <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
            <i class="icon-unfold mr-2"></i> Footer
        </button>
    </div>

    <div class="navbar-collapse collapse" id="navbar-footer">
        <span class="navbar-text">
						&copy; 2019. <a href="#">Trang Quản Trị Bất Động Sản</a> Design by <a href="https://cloudone.vn/" target="_blank">CloudOne</a>
					</span>

        
    </div>
</div>