<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        Navigation
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->

    <!-- Sidebar content -->
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user">
            <div class="card-body">
                <div class="media">
                    <div class="mr-3">
                        {{-- <a href="#"><img src="{{ asset('backend/global_assets/images/image.png')}}" width="38" height="38" class="rounded-circle" alt=""></a> --}}
                        <a href="#"><img src="{{ asset('uploads/avatar/'.Auth::guard('admin')->user()->image) }}" width="38" height="38" class="rounded-circle" alt=""></a>
                    </div>

                    <div class="media-body">
                        <div class="media-title font-weight-semibold">{{Auth::guard('admin')->user()->fullname}}</div>
                        <div class="font-size-xs opacity-50">
                          <i style="color: green" class="icon-primitive-dot"></i> &nbsp;Online
                        </div>
                    </div>

                    <div class="ml-3 align-self-center">
                        <a href="#" class="text-white"><i class="icon-cog3"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /user menu -->

        <!-- Main navigation -->
        <div class="card card-sidebar-mobile">
            @if(Auth::guard('admin')->user()->role ==1 )
                <ul class="nav nav-sidebar" data-nav-type="accordion">

                    <!-- Main -->
                    <li class="nav-item-header">
                        <div class="text-uppercase font-size-xs line-height-xs">ADMIN</div> <i class="icon-menu" title="Admin"></i></li>
                    <li class="nav-item">
                        <a href="{{url('admin')}}" class="nav-link active">
                            <i class="icon-home4"></i>
                            <span>
                                        Trang Chủ
                                    </span>
                        </a>
                    </li>
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="icon-people"></i> <span>Quản lý tài khoản</span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Tài khoản">
                            <li class="nav-item"><a href="{{route('useradmin.index')}}" class="nav-link">User Admin</a></li>
                            <li class="nav-item"><a href="{{route('member.index')}}" class="nav-link">Khách hàng</a></li>
                        </ul>
                    </li>
                     <li class="nav-item ">
                        <a href="{{route('menu.index')}}" class="nav-link"><i class="icon-menu3"></i> <span>Menu</span></a>
                    </li>
                    <li class="nav-item ">
                        <a href="{{route('slider.index')}}" class="nav-link"><i class="icon-images3"></i> <span>Slider</span></a>
                    </li>
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="icon-stack-text"></i> <span>Quản lý tin đăng</span></a>
                        
                        <ul class="nav nav-group-sub" data-submenu-title="Tin đăng">
                            <li class="nav-item"><a href="{{route('dmtd.index')}}" class="nav-link">Danh mục</a></li>
                            <li class="nav-item"><a href="{{route('tindang.index')}}" class="nav-link">Tin đăng</a></li>
                        </ul>
                    </li>
                   <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="icon-stack"></i> <span>Quản lý tin tức</span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Tin tức">
                            <li class="nav-item"><a href="{{route('dmtt.index')}}" class="nav-link">Danh mục</a></li>
                            <li class="nav-item"><a href="{{route('tintuc.index')}}" class="nav-link">Tin tức</a></li>
                        </ul>
                    </li>
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="icon-stats-dots"></i> <span>Quản lý dự án</span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Dự án">
                            <li class="nav-item"><a href="{{route('duan.index')}}" class="nav-link">Danh sách dự án</a></li>
                            <!-- <li class="nav-item"><a href="#" class="nav-link">Dự án</a></li> -->
                        </ul>
                    </li>
                    <li class="nav-item ">
                        <a href="{{route('tuyendung.index')}}" class="nav-link"><i class="icon-user-plus"></i> <span>Tuyển dụng</span></a>
                    </li>
                    <li class="nav-item ">
                        <a href="{{route('lienhe.index')}}" class="nav-link"><i class="icon-phone"></i> <span>Liên hệ</span></a>
                    </li>
                    <li class="nav-item ">
                        <a href="{{route('video.index')}}" class="nav-link"><i class="icon-video-camera2"></i> <span>Video</span></a>
                    </li>
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="icon-cog3"></i> <span>Cấu hình website</span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Cấu hình">
                            <li class="nav-item"><a href="{{route('gioithieu.index')}}" class="nav-link">Giới thiệu</a></li>
                            <li class="nav-item"><a href="{{route('logo.index')}}" class="nav-link">Logo - Banner</a></li>
                            <li class="nav-item"><a href="{{route('mangxahoi.index')}}" class="nav-link">Mạng xã hội</a></li>
                            <li class="nav-item"><a href="{{route('googlemap.index')}}" class="nav-link">Google Map</a></li>
                            <li class="nav-item"><a href="{{route('contactus.index')}}" class="nav-link">Liên hệ</a></li>
                            <li class="nav-item"><a href="{{route('footer.index')}}" class="nav-link">Footer</a></li>
                            <li class="nav-item"><a href="{{route('cauhinhkhac.index')}}" class="nav-link">Cấu hình khác</a></li>
                        </ul>
                    </li> 
                </ul>           
            @elseif(Auth::guard('admin')->user()->role ==2 )
                <ul class="nav nav-sidebar" data-nav-type="accordion">

                    <!-- Main -->
                    <li class="nav-item-header">
                        <div class="text-uppercase font-size-xs line-height-xs">ADMIN DUYỆT BÀI</div> <i class="icon-menu" title="Admin duyệt bài"></i></li>
                    <li class="nav-item">
                        <a href="{{url('admin')}}" class="nav-link active">
                            <i class="icon-home4"></i>
                            <span>
                                        Trang Chủ
                                    </span>
                        </a>
                    </li>
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="icon-stack-text"></i> <span>Quản lý tin đăng</span></a>
                        
                        <ul class="nav nav-group-sub" data-submenu-title="Tin đăng">
                           <li class="nav-item"><a href="{{url('admin/tindang/{tindang}')}}" class="nav-link">Tin đăng</a></li>
                        </ul>
                    </li>
                </ul>            
            @else
                <ul class="nav nav-sidebar" data-nav-type="accordion">

                    <!-- Main -->
                    <li class="nav-item-header">
                        <div class="text-uppercase font-size-xs line-height-xs">ADMIN POST TIN</div> <i class="icon-menu" title="Admin post bài"></i></li>
                    <li class="nav-item">
                        <a href="{{url('admin')}}" class="nav-link active">
                            <i class="icon-home4"></i>
                            <span>
                                        Trang Chủ
                                    </span>
                        </a>
                    </li> 
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="icon-stack-text"></i> <span>Quản lý tin đăng</span></a>
                        
                        <ul class="nav nav-group-sub" data-submenu-title="Tin đăng">
                            <li class="nav-item"><a href="{{route('tindang.create')}}" class="nav-link">Tin đăng</a></li>
                        </ul>
                    </li>
                </ul>
            @endif
        </div>
        <!-- /main navigation -->

    </div>
    <!-- /sidebar content -->

</div>