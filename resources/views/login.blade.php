{{-- <!DOCTYPE HTML>
<html lang="en">
<head>
<title>Admin Login From</title>
<!-- Meta tag Keywords -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Classic Login Form Responsive Widget,Login form widgets, Sign up Web forms , Login signup Responsive web form,Flat Pricing table,Flat Drop downs,Registration Forms,News letter Forms,Elements" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Meta tag Keywords -->

<!-- css files -->
<link rel="stylesheet" href="{{asset('login/css/style.css')}}" type="text/css" media="all" /> <!-- Style-CSS --> 
<link rel="stylesheet" href="{{asset('login/css/font-awesome.css')}}"> <!-- Font-Awesome-Icons-CSS -->

<!-- //css files -->

<!-- js -->
<script type="text/javascript" src="{{asset('login/js/jquery-2.1.4.min.js')}}"></script>
<!-- //js -->

<!-- online-fonts -->
<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
<!-- <link href="//fonts.googleapis.com/css?family=Oleo+Script:400,700&amp;subset=latin-ext" rel="stylesheet"> -->
<!-- //online-fonts -->
</head>
<body>
<script src='//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script><script src="//m.servedby-buysellads.com/monetization.js" type="text/javascript"></script>
<script>
(function(){
  if(typeof _bsa !== 'undefined' && _bsa) {
      // format, zoneKey, segment:value, options
      _bsa.init('flexbar', 'CKYI627U', 'placement:w3layoutscom');
    }
})();
</script>
<script>
(function(){
if(typeof _bsa !== 'undefined' && _bsa) {
  // format, zoneKey, segment:value, options
  _bsa.init('fancybar', 'CKYDL2JN', 'placement:demo');
}
})();
</script>
<script>
(function(){
  if(typeof _bsa !== 'undefined' && _bsa) {
      // format, zoneKey, segment:value, options
      _bsa.init('stickybox', 'CKYI653J', 'placement:w3layoutscom');
    }
})();
</script>

  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-125810435-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-125810435-1');
</script><script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-30027142-1', 'w3layouts.com');
  ga('send', 'pageview');
</script>
<body>
<script src="{{asset('login/js/jquery.vide.min.js')}}"></script>
  <!-- main -->
  <div data-vide-bg="login/video/Ipad">
    <div class="center-container">
      <!--header-->
      <div class="header-w3l">
        <h1>Đăng Nhập Trang Quản Trị</h1>
      </div>
      <!--//header-->
    <!---728x90--->
 
      <div class="main-content-agile">
        <div class="sub-main-w3"> 
          <div class="wthree-pro">
            <h2>Nhập Thông Tin</h2>
          </div>
          <div style="color: red">
            @include('admin.blocks.alert')
          </div>

                   @if ($errors->any())
                      <div class="alert alert-danger">
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif
          <form action="" method="post">
            @csrf
            <input placeholder="E-mail" name="email" class="user" type="email" required="">
            <span class="icon1"><i class="fa fa-user" aria-hidden="true"></i></span><br><br>
            <input  placeholder="Password" name="password" class="pass" type="password" required="">
            <span class="icon2"><i class="fa fa-unlock" aria-hidden="true"></i></span><br>
            <div class="sub-w3l">
           
             <h6><a href="{{ route('admin-password.request') }}">Forgot Password?</a></h6>
              <div class="right-w3l">
                <input type="submit" value="Đăng Nhập">
              </div>
            </div>
          </form>
        </div>
      </div>
      <!--//main-->
    <!---728x90--->

      <!--footer-->
      <div class="footer">
        <p>Bản quyền © 2019 | Design by <a href="https://cloudone.vn/">CloudOne</a></p>
      </div>
      <!--//footer-->
    <!---728x90--->

    </div>
  </div>

</body>
</html>
 --}}