<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Member extends Model 
{
   
   protected $table = "bds_member";
   public $timestamp = false;

   public function dbmember()
   {
   		return DB::table('bds_member');
   }
   public function listmember()
   {
   		return $this->dbmember()->orderBy('id','DESC')->get()->toArray();
   }
   public function addMember($data)
   {
   		return $this->dbmember()->insert($data);
   }
   public function editmember($id)
   {
   		return $this->dbmember()->find($id);
   }
   public function updatemember($data,$id)
   {
   		return $this->dbmember()->whereId($id)->update($data);
   }
   public function delmember($data)
   {
   		return $this->dbmember()->delete($data);
   }

   public function updateToken($data,$token)
   {
      return $this->dbmember()->where('reg_token',$token)->update($data);
   }
}
