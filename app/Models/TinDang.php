<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class TinDang extends Model
{
    protected $table = 'bds_tindang';
    public $timestamp = false;
    public function dbPostings()
    {
    	return DB::table('bds_tindang');
    }
   	public function listPostings()
   	{
   		return $this->dbPostings()->orderBy('id', 'DESC')->get()->toArray();
   	}
    public function addPostings($data)
    {
    	return $this->dbPostings()->insert($data);
    }
    public function editPostings($id)
    {
    	return $this->dbPostings()->find($id);
    }
    public function updatePostings($data,$id)
    {
    	return $this->dbPostings()->whereId($id)->update($data);
    }
    public function delPostings($data)
    {
    	return $this->dbPostings()->delete($data);
    }




    public function editTinDangByAlias($alias)
    {
        return $this->dbPostings()->where('alias',$alias)->first();
    }
}
