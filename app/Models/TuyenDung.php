<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class TuyenDung extends Model
{
    protected $table = 'bds_tuyendung';
    public $timestamp = false;
    public function dbtuyendung()
    {
    	return DB::table('bds_tuyendung');
    }
    public function listtuyendung()
    {
    	return $this->dbtuyendung()->orderBy('id','DESC')->get()->toArray();
    }
    public function addtuyendung($data)
    {
    	return $this->dbtuyendung()->insert($data);
    }
    public function edittuyendung($id)
    {
    	return $this->dbtuyendung()->find($id);
    }
    public function updatetuyendung($data,$id)
    {
    	return $this->dbtuyendung()->whereId($id)->update($data);
    }
    public function deltuyendung($data)
    {
    	return $this->dbtuyendung()->delete($data);
    }
}
