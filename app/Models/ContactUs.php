<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class ContactUs extends Model
{
    protected $table = 'bds_config';
    public $timestamp = false;
    public function dbContactsUs()
    {
    	return DB::table('bds_config');
    }
  	public function editContactUs($id)
  	{
  		return $this->dbContactsUs()->select('content','phonename')->find($id);
  	}
    public function updateHotLine1($data)
    {
      return $this->dbContactsUs()->whereId(9)->update($data);
    }
     public function updatePhoneName1($data)
    {
      return $this->dbContactsUs()->whereId(9)->update($data);
    }
    public function updateHotLine2($data)
    {
      return $this->dbContactsUs()->whereId(10)->update($data);
    }
    public function updatePhoneName2($data)
    {
      return $this->dbContactsUs()->whereId(10)->update($data);
    }
    public function updateHotLine3($data)
    {
      return $this->dbContactsUs()->whereId(11)->update($data);
    }
    public function updatePhoneName3($data)
    {
      return $this->dbContactsUs()->whereId(11)->update($data);
    }
}
