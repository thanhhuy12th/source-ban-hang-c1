<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class DuAn extends Model
{
   protected $table = "bds_duan";
   public $timestamp = false;
   public function db()
   {
   		return DB::table('bds_duan');
   }
   public function listduan()
   {
   		return $this->db()->orderBy('id','DESC')->get()->toArray();
   }
   public function addduan($data)
   {
   		return $this->db()->insert($data);
   }
   public function editduan($id)
   {
   		return $this->db()->find($id);
   }
   public function updateduan($data,$id)
   {
   		return $this->db()->whereId($id)->update($data);
   }
   public function delduan($data)
   {
   		return $this->db()->delete($data);
   }
}
