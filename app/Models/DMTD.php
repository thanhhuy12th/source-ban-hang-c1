<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class DMTD extends Model
{
    protected $table = "bds_dmtd";
    public $timestamp = false;

    public function db()
    {
    	return DB::table('bds_dmtd');
    }
    public function listcate()
    {
    	return $this->db()->get()->toArray();
    }
    public function addcate($data)
    {
    	return $this->db()->insert($data);
    }
    public function showEdit($id)
    {
    	return $this->db()->find($id);
    }
    public function updatecate($data,$id)
    {
    	return $this->db()->whereId($id)->update($data);
    }
    public function delcate($data)
    {
    	return $this->db()->delete($data);
    }

    // option
    public function getParent()
    {  
        return $this->db()->get()->toArray();
    }
    
    public function listCateByIdParent($id) 
    {
        return $this->db()->where("parent_id",$id)->get()->toArray();
    }

}
