<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Configuration extends Model
{
    protected $table = 'bds_config';
    public $timestamp = false;
    public function dbConfig()
    {
    	return DB::table('bds_config');
    }
    public function editConfig($id)
    {
    	return $this->dbConfig()->select('content')->find($id);
    }
    public function updatehotline($data)
    {
    	return $this->dbConfig()->whereId(16)->update($data);
    }
    public function updateEmail($data)
    {
    	return $this->dbConfig()->whereId(17)->update($data);
    }
    public function updateSEO($data)
    {
    	return $this->dbConfig()->whereId(18)->update($data);
    }
    public function updateDescription($data)
    {
    	return $this->dbConfig()->whereId(19)->update($data);
    }
    public function updateTitle($data)
    {
    	return $this->dbConfig()->whereId(20)->update($data);
    }
    public function updateBaseUrl($data)
    {
    	return $this->dbConfig()->whereId(21)->update($data);
    }
    public function updateAddress($data)
    {
    	return $this->dbConfig()->whereId(22)->update($data);
    }
    public function updateCustomerVip($data)
    {
    	return $this->dbConfig()->whereId(23)->update($data);
    }
    public function updateUsername($data)
    {
    	return $this->dbConfig()->whereId(24)->update($data);
    }
    public function updatePassword($data)
    {
    	return $this->dbConfig()->whereId(25)->update($data);
    }
    public function updateChatbox($data)
    {
    	return $this->dbConfig()->whereId(26)->update($data);
    }
}
