<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Footer extends Model
{
    protected $table = 'bds_config';
    public $timestamp = false;
    public function dbFooter()
    {
    	return DB::table('bds_config');
    }
    public function editFooter($id)
    {
    	return $this->dbFooter()->select('content')->find($id);
    }
    public function updateFooter($data)
    {
    	return $this->dbFooter()->whereId(15)->update($data);
    }
}
