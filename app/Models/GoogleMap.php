<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class GoogleMap extends Model
{
    protected $table = 'bds_config';
    public $timestamp = false;
    public function dbGGMap()
    {
    	return DB::table('bds_config');
    }
    public function editGGmap($id)
    {
    	return $this->dbGGMap()->select('content')->find($id);
    }
    public function updateGGmap($data)
    {
    	return $this->dbGGMap()->whereId(14)->update($data);
    }
}
