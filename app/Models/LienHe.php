<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class LienHe extends Model
{
   protected $table = "bds_lienhe";
   public $timestamp = false;

   public function dblienhe()
   {
   		return DB::table('bds_lienhe');
   }
   public function listcontact()
   {
   		return $this->dblienhe()->orderBy('id','DESC')->get()->toArray();
   }
   public function addcontact($data)
   {
   		return $this->dblienhe()->insert($data);
   }
   public function editcontact($id)
   {
   		return $this->dblienhe()->find($id);
   }
   public function updatecontact($data,$id)
   {
   		return $this->dblienhe()->whereId($id)->update($data);
   }
   public function delcontact($data)
   {
   		return $this->dblienhe()->delete($data);
   }

}
