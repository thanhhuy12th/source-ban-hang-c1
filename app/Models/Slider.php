<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Slider extends Model
{
    protected $table = 'bds_slider';
    public $timestamp = false;
    public function dbslider()
    {
    	return DB::table('bds_slider');
    }
    public function listSlider()
    {
    	return $this->dbslider()->orderBy('id','DESC')->get()->toArray();
    }
    public function addSlider($data)
    {
    	return $this->dbslider()->insert($data);
    }
    public function editSlider($id)
    {
    	return $this->dbslider()->find($id);
    }
    public function updateSlider($data,$id)
    {
    	return $this->dbslider()->whereId($id)->update($data);
    }
    public function delSlider($data)
    {
    	return $this->dbslider()->delete($data);
    }
}
