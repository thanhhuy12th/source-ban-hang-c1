<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
      
        View::composer(
            '*', 'App\Http\View\Composers\HeaderComposer'
        );
        View::composer(
            [
                'frontend.pages.get-category-parent',
                'frontend.quanlytindang',
                'frontend.pages.timkiem_category',
                'frontend.change-password',
                'frontend.quanlytk',
                'frontend.blocks.menu',
                'frontend.pages.project_news',
                'frontend.pages.product_category',
                'frontend.pages.product_detail',
                'frontend.blocks.footer',
                'admin.modules.tindang.create',
                'admin.modules.tindang.edit',
            ],
            
             'App\Http\View\Composers\MenuComposer'    
        );
         View::composer(
            [
                'frontend.pages.get-category-parent',
                'frontend.pages.timkiem_category',
                'frontend.pages.project_news',
                'frontend.pages.product_category',
                'frontend.pages.product_detail',
                'frontend.blocks.menu',
                'frontend.pages.home',
                'frontend.blocks.footer',

            ],

             'App\Http\View\Composers\HomeComposer'
        );
         View::composer(
            [
                'frontend.pages.lienhecongty',
                'frontend.blocks.lienhe',
                'frontend.index',
                'frontend.blocks.footer',
                'frontend.pages.contactUs',
            ],
                  'App\Http\View\Composers\FooterComposer'    
        );
         View::composer(
            [
                'frontend.pages.get-category-parent',
                'frontend.pages.timkiem_category',
                'frontend.pages.news_category',
                'frontend.pages.tintuc',
                'frontend.pages.project_news',
                'frontend.pages.product_category',
                'frontend.pages.product_detail'
                
            ],
                 'App\Http\View\Composers\RelatedpostComposer'    
        );
          View::composer(
            'admin.dashboard.index','App\Http\View\Composers\DashboardComposer'
        );


    }
}
