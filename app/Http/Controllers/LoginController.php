<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function getLogin()
    {
    	return view('auth.admin.login');
    }
    public function clear_cache()
    { 
        Artisan::call('optimize');
        Artisan::call('cache:clear');
        Artisan::call('route:cache');
        Artisan::call('view:clear');
        Artisan::call('config:cache');
        return "All Done!";
        //return view('login');
    }
    public function postLogin(Request $request)
    {
    	$credentials = $request->only('username', 'password');

        if (Auth::guard('admin')->attempt($credentials)) {
            
            return redirect('./admin');

        } else {

        	return redirect()->back()->with('error',"Mật khẩu không đúng");
        }
    }
    public function getLogout()
    {
    	Auth::guard('admin')->logout();

    	return redirect()->route('getLogin');
    }

    public function isAdmin()
    {
        if(Auth::guard('admin')->user()->role == 2 || Auth::guard('admin')->user()->role == 3 || Auth::guard('admin')->user()->role == null) {
            Auth::guard('admin')->logout();
            return redirect()->route('getLogin');
        }
    }
}
