<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Login\LoginStore;
use App\Http\Requests\SignUp\SignupStore;
use App\Http\Requests\TinDang\PostingsRequest;
use App\Models\TinDang;
use App\Models\DMTD;
use App\Models\Member;
use App\Http\Controllers\EmailController;
use Auth;
use Hash;
use DB,Mail,DateTime;


class HomeController extends Controller
{
    private $tindang;
    private $danhMucTinDang;
    private $member;
    private $reg;
    public function __construct(
        TinDang $tindang, 
        DMTD $danhMucTinDang,
        Member $member,
        EmailController $reg
    )
    {
        $this->postings = $tindang;
        $this->danhMucTinDang = $danhMucTinDang;
        $this->member = $member;
        $this->reg = $reg;
    }
    public function delImages($id)
    {
        $item = $this->member->editmember($id);
        $filename = 'public/uploads/avatar'.$item->image;
        if (!is_null($item->image)) {
             if(\File::exists($filename)){
            \File::delete($filename);
            }  
        }
       
    }
     public function DelImg($id)
    {
        $item = $this->postings->editPostings($id);
        $filename = 'public/uploads/postings'.$item->image;
        if (!is_null($item->image)) {
             if(\File::exists($filename)){
            \File::delete($filename);
            }  
        }
    }
    public function Changehinh($id)
    {
        $item = $this->postings->editPostings($id);
        $splittedstring=explode("||",$item->hinh);
        foreach ($splittedstring as $key) {
            $filename = 'public/uploads/postings/'.$key;
            if (!is_null($splittedstring)) {
                 if(\File::exists($filename)){
                \File::delete($filename);
                }  
            }
        }
       
    }
    public function index()
    {
        
    	return view('frontend.pages.home');
    }

    // tin dang
    public function product_category($alias,$parent)
    {
        $getID = DB::table('bds_dmtd')->select('id')->where('parent_id',$parent)->where('alias',$alias)->get();

        $data['product_category_data'] = DB::table('bds_tindang')->where('duyetbai',1)->where('status',1)->where('category_id', $getID[0]->id)->orderBy('id','DESC')->limit(8)->paginate(6);
    	
    	return view('frontend.pages.product_category',$data);
    }
    public function get_category_parent($alias)
    {
        $data['alias'] = $alias;
        
        return view('frontend.pages.get-category-parent',$data);
      
    }
    public function product_detail($alias)
    {
        $data['cates_detail'] = DB::table('bds_tindang')->where('alias',$alias)->first();
        return view('frontend.pages.product_detail',$data);


    }

    // gioi thieu
    public function about()
    {
    	$data['gioithieu'] = DB::table('bds_config')->whereId(4)->first();
      $data['title'] = DB::table('bds_config')->whereId(20)->first();
    	$data['video'] = DB::table('bds_video')->orderBy('id', 'DESC')->first();
    	$data['hotline1'] = DB::table('bds_config')->whereId(9)->first();
    	$data['hotline2'] = DB::table('bds_config')->whereId(10)->first();
    	$data['hotline3'] = DB::table('bds_config')->whereId(11)->first();
      $data['address'] = DB::table('bds_config')->whereId(22)->first();
      $data['email'] = DB::table('bds_config')->whereId(17)->first();
      $data['link'] = DB::table('bds_config')->whereId(21)->first();
      $data['ggmap'] = DB::table('bds_config')->whereId(14)->first();

    	return view('frontend.pages.gioithieu',$data);
    }

    // du an
    public function project_news()
    {
        $data['project'] = DB::table('bds_duan')->where('status',1)->orderBy('id','DESC')->limit(10)->paginate(8); 

        return view('frontend.pages.project_news',$data);
    }
    public function project_detail($alias)
    {
        $data['project_detail'] = DB::table('bds_duan')->where('alias',$alias)->first();
       
        return view('frontend.pages.project_detail',$data);
    }

    // tin tuc
    public function getNews()
    {
        $data['news'] = DB::table('bds_tintuc')->orderBy('id', 'DESC')->limit(10)->paginate(6);
        
        return view('frontend.pages.tintuc',$data);
    }
    public function news_detail($alias)
    {
        $data['chitiettin'] = DB::table('bds_tintuc')->where('alias',$alias)->first();

        return view('frontend.pages.news_detail',$data);
    }
    public function newsCategory($id)
    {
        $data['news_category'] = DB::table('bds_tintuc')->where('parent_id',$id)->orderBy('id','DESC')->limit(10)->paginate(6);
        
        return view('frontend.pages.news_category',$data);
    }

    //tuyen dung
    public function recruitment()
    {
        $data['tuyendung'] = DB::table('bds_tuyendung')->orderBy('id', 'DESC')->limit(10)->paginate(6);

        return view('frontend.pages.tuyendung',$data);
    }
    public function tuyendung_detail($alias)
    {
        $data['chitiettuyendung'] = DB::table('bds_tuyendung')->where('alias',$alias)->first();

        return view('frontend.pages.tuyendung_detail',$data);
    }

    // Liên hệ
    public function ContactCompany()
    {
        return view('frontend.pages.lienhecongty');
    }
    // lien he với chúng tôi
    public function contactUs()
    {
        return view('frontend.pages.contactUs');
    }
    public function storeContact(Request $request)
    {
        $captcha = null;
    if(isset($_POST['g-recaptcha-response'])){
        $captcha=$_POST['g-recaptcha-response'];
    }
    if(!$captcha){
      echo "<script>
      alert('Chưa nhập mã xác thực!');
      window.location = '".url('/contact')."'
      </script>";
  }
  else {
    $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LeoSL4UAAAAAOMMwPOu1WgZtDhmRX8dCq_VC-nm&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
    $obj =json_decode($response); 
    if($obj->{'success'}==true)            
    {
        $data = array(
            'name'      => $request->name,
            'email'     => $request->email,
            'phone'     => $request->phone,
            'content'   => $request->content,
            'created_at'=> new DateTime 
        );
        DB::table('bds_lienhe')->insert($data);

        echo "<script>
        alert('Cảm ơn bạn đã liên hệ. Chúng tôi sẽ liên hệ lại với bạn trong thời gian sớm nhất');
        window.location = '".url('/')."'
        </script>";
    }
    else {
        echo "<script>
        alert('Sai mã xác thực, vui lòng nhập lại');
        window.location = '".url('/lien-he')."'
        </script>";

    }
}      
        // $data = array(
        //     'name'      => $request->name,
        //     'email'     => $request->email,
        //     'phone'     => $request->phone,
        //     'content'   => $request->content,
        //     'created_at'=> new DateTime 
        // );

        // DB::table('bds_lienhe')->insert($data);

        // echo "<script>
        //        alert('Cảm ơn bạn đã liên hệ. Chúng tôi sẽ liên hệ lại với bạn trong thời gian sớm nhất');
        //        window.location = '".url('/')."'
        //  </script>";
         
    }
   

    // dang nhap
    public function getDangnhap()
    {
        return view('frontend.login-user');
    }
    public function postDangnhap(LoginStore $request)
    {
                  $credentials = [
                      'email'     => $request['email'],
                      'password'  => $request['password'],
                      'reg_status'  => 1,
                  ];
                  if (Auth::attempt($credentials) ){

                      return redirect('quan-ly-tai-khoan');
                 }

              $captcha = null;
              if(isset($_POST['g-recaptcha-response'])){
                  $captcha=$_POST['g-recaptcha-response'];
              }
              if(!$captcha){
                echo "<script>
                alert('Chưa nhập mã xác thực!');
                window.location = '".url('/dangnhap')."'
                </script>";
            }
            else {
              $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LeoSL4UAAAAAOMMwPOu1WgZtDhmRX8dCq_VC-nm&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
              $obj =json_decode($response); 
              if($obj->{'success'}==true)            
              {
                  $credentials = [
                      'email'     => $request['email'],
                      'password'  => $request['password'],
                      'reg_status'  => 1,
                  ];
                  if (Auth::attempt($credentials) ){

                     return redirect('quan-ly-tai-khoan');

                 } else {

                     echo "<script>
                     alert('Mật khẩu không đúng, vui lòng nhập lại');
                     window.location = '".url('/dangnhap')."'
                     </script>";


                 }
             }
             else {

                 echo "<script>
                 alert('Sai mã xác thực, vui lòng nhập lại');
                 window.location = '".url('/dangnhap')."'
                 </script>";


             }
          }      

       
       // $credentials = [
       //      'email'     => $request['email'],
       //      'password'  => $request['password'],
       //      'reg_status'  => 1,
       //  ];
        
       //  if (Auth::attempt($credentials) ){

       //             echo "<script>
       //              alert('Đăng nhập thành công !');
       //              window.location = '".url('/quan-ly-tai-khoan')."'
       //              </script>";
       //  } else {

       //      return redirect()->route('dangnhap')->with('error',"message.dangnhap_required");
       //  }

        
    }

    // dang xuat
    public function getDangxuat()
    {
        Auth::logout();

        return redirect()->route('home');
    }
     // dang ky tai khoan
    public function getDangky()
    {
        return view('frontend.sign-up');
    }
    public function postDangky(Request $request)
    {
         $captcha = null;
          if(isset($_POST['g-recaptcha-response'])){
              $captcha=$_POST['g-recaptcha-response'];
          }
          if(!$captcha){
            echo "<script>
            alert('Chưa nhập mã xác thực!');
            window.location = '".url('/dangky')."'
            </script>";
        }
        else {
          $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LeoSL4UAAAAAOMMwPOu1WgZtDhmRX8dCq_VC-nm&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
          $obj =json_decode($response); 
          if($obj->{'success'}==true)            
          {
              $now        = new DateTime();
              $reg_token      = md5($now->format('Y-m-d HH:mm:ss').$request->email.rand());
              $urlBase = 'http://ntkland.vn/'; 
              $data = array(
                  'phone'         => $request->phone,
                  'email'         => $request->email,
                  'password'      => bcrypt($request->password),
                  'reg_status'    => 0,
                  'reg_token'     => $reg_token,
                  'created_at'    => new DateTime
              );
              $Content = 'Chào bạn, <p>Bạn cần xác thực email để đăng nhập</p> <p>Bạn vui lòng click vào đường dẫn bên dưới để xác thực tài khoản.</p>'.$urlBase.'register?regtoken='.$reg_token.' <p>Trân trọng,</p><p>MUANHAVN.NET</p>';

              $this->reg->register_account($Content, $request->email);

              $register =  DB::table('bds_member')->insert($data);

              if ($register) {

                  echo "<script>
                  alert('Chào bạn, Bạn vui lòng kích hoạt email để đăng nhập vào tài khoản.');
                  window.location = '".url('/dangnhap')."'
                  </script>";
              } 
              else {

               echo "<script>
               alert('Sai mã xác thực, vui lòng nhập lại');
               window.location = '".url('/dangky')."'
               </script>";

           }   
          }
        }   
                     //  $now        = new DateTime();
                     //  $reg_token      = md5($now->format('Y-m-d HH:mm:ss').$request->email.rand());
                     //  $urlBase = 'http://ntkland.vn/';
                     //  $data = array(
                     //      'phone'         => $request->phone,
                     //      'email'         => $request->email,
                     //      'password'      => bcrypt($request->password),
                     //      'reg_status'    => 0,
                     //      'reg_token'     => $reg_token,
                     //      'created_at'    => new DateTime
                     //  );
                     //  $Content = 'Chào bạn, <p>Bạn cần xác thực email để đăng nhập</p> <p>Bạn vui lòng click vào đường dẫn bên dưới để xác thực tài khoản.</p>'.$urlBase.'register?regtoken='.$reg_token.' <p>Trân trọng,</p><p>NTK Land</p>';
                    
                     //  $this->reg->register_account($Content, $request->email);

                     // $register =  DB::table('bds_member')->insert($data);
                     
                     // if ($register) {

                     //  echo "<script>
                     //         alert('Chào bạn, Bạn vui lòng kích hoạt email để đăng nhập vào tài khoản.');
                     //         window.location = '".url('dangnhap')."'
                     //   </script>";

                     // } else {

                     //  return back()->with('Đăng ký thất bại.Vui lòng đăng ký lại.');
                     // }        
                     
    }
    public function Registersucces()
    {

        return view('auth.email.register');
    }
    public function RegisterAccount()
    {
        $reg_token = \Illuminate\Support\Facades\Request::post('regtoken');
        $data = array(
            'reg_status'    => 1,
        );
        $register =  $this->member->updateToken($data,$reg_token);

        return view('auth.email.register');
    }
    
    // quan ly tai khoan
    
    public function create()
    {
       
        $data['cate_option'] = [];

        // Xử lý option danh mục tin đăng
        $option_parent = $this->danhMucTinDang->listCateByIdParent(0); // Set 0 để lây danh mục cha
        foreach ($option_parent as $key => $value) {
            // Tạo mảng 2 chiều
            $option_item = ["id"=>$value->id,"name"=>$value->name];
            // Add mảng 2 chiều vào mảng 1 chiều tổng
            array_push($data['cate_option'], $option_item);

            // Lấy các danh mục con thêm vào mảng 1 chiều tổng
            $option_child = $this->danhMucTinDang->listCateByIdParent($value->id);
            
            foreach ($option_child as $keyC => $valueC) {
                // Tạo mảng 2 chiều danh mục con
                $option_child_item = ["id"=>$valueC->id,"name"=>"-- ".$valueC->name];
                array_push($data['cate_option'], $option_child_item);
            }
        }        

        return view('frontend.quanlytindang',$data);
    }

    public function ImageOne($request)
	{
	$img = "default.jpg" ;
	if ($request->hasFile('image')) {
	            $image = $request->file('image');
	            $image_name = time(). '-'.$image->getClientOriginalName();
	            $des = 'public/uploads/postings';
	            $image->move($des,$image_name);
	            $img1=$image_name;
	            
	            return $img1;
	    }
	    return $img;
	}
    public function store(Request $request)
    {
        $img1="default.jpg";
        $fileNameTotal = "";
        $imgOne = $this->ImageOne($request);
        if ($request->hasFile('hinh')) {

            $rules = array("jpg","jpeg","png","jpeg","gif","svg");
             foreach ($request->file('hinh') as $file) {
                $fileExtension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
                if (in_array($fileExtension,$rules)) {
                    $arr = [];
                     $fileName = time().'_'.$file->getClientOriginalName();
                     $uploadPath = 'public/uploads/postings';
                     $file->move($uploadPath, $fileName);
                    $fileNameTotal .= $fileName."||";

	             }
	        }
	        $contact = $request->name_contact . "||" . $request->email_contact . "||" . $request->phone_contact . "||" . $request->zalo_contact;
            $vitrinew = str_replace( ' width="600" height="450" ', 'width="1000" height="450', $request->vitridacdia );
	       	$data = array(
	                        'title'                 => $request->title,
	                        'alias'                 => (empty($request->alias))? str_slug($request->title) : $request->alias,
                          'dientichmatbang'       => $request->dientichmatbang,
                          'dientichsudung'        => $request->dientichsudung,
                          'price'                 => $request->price,
                          'typegia'               => $request->typegia,
	                        'vitri'                 => $request->vitri,
	                        'province'              => $request->province,
	                        'district'              => $request->district,
	                        'intro'                 => $request->intro,
	                        'tongquan'              => $request->tongquan,
	                        'vitridacdia'           => $vitrinew,
	                        'typegia'               => $request->typegia,
	                        'contact'               => $contact,
	                        'category_id'           => $request->category_id,
	                        'member_id'             => Auth::user()->id,
	                        'hinh'                  => $fileNameTotal,
	                        'image'                 => $imgOne,
	                        'created_at'            => new DateTime
	                    );  
	        $this->postings->addPostings($data);
	        return redirect()->back()->with('success',"message.store_required");
	    } else{
	        $img1="default.jpg";
	        $contact = $request->name_contact . "||" . $request->email_contact . "||" . $request->phone_contact . "||" . $request->zalo_contact;
            $vitrinew = str_replace( ' width="600" height="450" ', 'width="1000" height="450', $request->vitridacdia );
	        $data = array(
	                    'title'                 => $request->title,
                      'alias'                 => (empty($request->alias))? str_slug($request->title) : $request->alias,
                      'dientichmatbang'       => $request->dientichmatbang,
                      'dientichsudung'        => $request->dientichsudung,
                      'price'                 => $request->price,
                      'typegia'               => $request->typegia,
                      'vitri'                 => $request->vitri,
                      'province'              => $request->province,
                      'district'              => $request->district,
                      'intro'                 => $request->intro,
                      'tongquan'              => $request->tongquan,
                      'vitridacdia'           => $vitrinew,
                      'typegia'               => $request->typegia,
                      'contact'               => $contact,
	                    'category_id'           => $request->category_id,
	                    'member_id'             => Auth::user()->id,
	                    'hinh'                  => $img1,
	                    'image'                 => $imgOne,
	                    'created_at'            => new DateTime
	                );   
	   		$this->postings->addPostings($data);
	     	return redirect()->back()->with('success',"message.store_required");
	    } 

    }
    public function edit($alias)
    {
        $data['post'] = $this->postings->editTinDangByAlias($alias);

         $data['cate_option'] = [];

        // Xử lý option danh mục tin đăng
        $option_parent = $this->danhMucTinDang->listCateByIdParent(0); // Set 0 để lây danh mục cha
        foreach ($option_parent as $key => $value) {
            // Tạo mảng 2 chiều
            $option_item = ["id"=>$value->id,"name"=>$value->name];
            // Add mảng 2 chiều vào mảng 1 chiều tổng
            array_push($data['cate_option'], $option_item);

            // Lấy các danh mục con thêm vào mảng 1 chiều tổng
            $option_child = $this->danhMucTinDang->listCateByIdParent($value->id);
            
            foreach ($option_child as $keyC => $valueC) {
                // Tạo mảng 2 chiều danh mục con
                $option_child_item = ["id"=>$valueC->id,"name"=>"-- ".$valueC->name];
                array_push($data['cate_option'], $option_child_item);
            }
        }

        return view('frontend.editquanlytd',$data);
    }

    public function UpdateImageOne($request)
    {
    $mothinh = $request->imagehidden;
    if ($request->hasFile('image')) {
                $image = $request->file('image');
                $image_name = time(). '-'.$image->getClientOriginalName();
                $des = 'public/uploads/postings';
                $image->move($des,$image_name);
                $img1=$image_name;
                
                return $img1;
        }
        return $mothinh;
    }


    public function update(Request $request, $alias)
    {
       $item = $this->postings->editTinDangByAlias($alias);
        $img1="default.jpg";
        $fileNameTotal = "";
       
        if ($request->hasFile('hinh')== true) {
            $rules = array("jpg","jpeg","png","jpeg","gif","svg");
             foreach ($request->file('hinh') as $file) {
                $fileExtension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
                if (in_array($fileExtension,$rules)) {
                    $arr = [];
                     $fileName = time().'_'.$file->getClientOriginalName();
                     $uploadPath = 'public/uploads/postings';
                     $file->move($uploadPath, $fileName);
                    $this->Changehinh($id);
                    $fileNameTotal .= $fileName."||";
            }
        }

         $contact = $request->name_contact . "||" . $request->email_contact . "||" . $request->phone_contact . "||" . $request->zalo_contact ;
             $data = array(
                        'title'                 => $request->title,
                        'alias'                 => (empty($request->alias))? str_slug($request->title) : $request->alias,
                        'dientichmatbang'       => $request->dientichmatbang,
                        'dientichsudung'        => $request->dientichsudung,
                        'price'                 => $request->price,
                        'typegia'               => $request->typegia,
                        'vitri'                 => $request->vitri,
                        'province'              => $request->province,
                        'district'              => $request->district,
                        'intro'                 => $request->intro,
                        'tongquan'              => $request->tongquan,                       
                        'vitridacdia'           => $request->vitridacdia,
                        'contact'               => $contact,
                        'category_id'           => $request->category_id,
                        'member_id'             => Auth::user()->id,
                        'hinh'                  => $fileNameTotal,
                        'image'                 => $this->UpdateImageOne($request),
                        'created_at'            => new DateTime
                    ); 
          $this->postings->updatePostings($data,$id);   

          return redirect()->back()->with('success',"message.update_required");

         } else {
            $contact = $request->name_contact . "||" . $request->email_contact . "||" . $request->phone_contact . "||" . $request->zalo_contact;
            $data = array(
                        'title'                 => $request->title,
                        'alias'                 => (empty($request->alias))? str_slug($request->title) : $request->alias,
                        'dientichmatbang'       => $request->dientichmatbang,
                        'dientichsudung'        => $request->dientichsudung,
                        'price'                 => $request->price,
                        'typegia'               => $request->typegia,
                        'vitri'                 => $request->vitri,
                        'province'              => $request->province,
                        'district'              => $request->district,
                        'intro'                 => $request->intro,
                        'tongquan'              => $request->tongquan,
                        'vitridacdia'           => $request->vitridacdia,
                        'contact'               => $contact,
                        'category_id'           => $request->category_id,
                        'member_id'             => Auth::user()->id,
                        'hinh'                  => $request->hinhhidden,
                        'image'                 => $this->UpdateImageOne($request),
                        'created_at'            => new DateTime
                    );       
            //dd($data);
        $this->postings->updatePostings($data,$id);

        return redirect()->back()->with('success',"message.update_required");
            
         } 
        
         return redirect()->back()->with('error', 'Cập nhật không thành công!');

    }


    public function editQuanlytk()
    {
        
       $data['postnews'] = DB::table('bds_tindang')
            ->select('bds_tindang.*')
            ->join('bds_dmtd', 'bds_tindang.category_id', '=', 'bds_dmtd.id')
            ->join('bds_member', 'bds_tindang.member_id', '=', 'bds_member.id')
            ->where('bds_tindang.member_id', '=',Auth::user()->id)
            ->where('bds_tindang.user_type', 2)
            ->paginate(10);
        return view('frontend.quanlytk',$data);
    }

    public function updateQuanlytk(Request $request)
    {
        
        $data = array(
            'fullname'      => $request->fullname,
            'username'      => $request->username,
            'email'         => $request->email,
            'phone'         => $request->phone,
            'address'       => $request->address,
            'created_at'    => new DateTime
        );

       if($request->hasFile('image')){
                $image = $request->file('image');
                $image_name = time(). '-'.$image->getClientOriginalName();
                $des = 'public/uploads/avatar';
                $image->move($des,$image_name);
                $data['image']=$image_name;
                $this->delImages(Auth::user()->id);
            }

           // dd($data);
     DB::table('bds_member')->whereId(Auth::user()->id)->update($data);

        echo "<script>
                    alert('Thông tin của bạn đã được thay đổi thành công.');
                    window.location = '".url('/')."'
              </script>";
    }

    public function destroyQuanlytd($id)
    {
       DB::table('bds_tindang')->whereId($id)->delete();
       
        return back()->with("success", "message.delete_required");
       
    }

     // doi mat khau
     
    public function updateChangePassword(Request $request)
    {
         if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // Mật khẩu trùng khớp
            
            return redirect()->back()->with("error","Mật khẩu hiện tại của bạn không khớp với mật khẩu bạn cung cấp. Vui lòng thử lại.");
        }
        if(Hash::check($request->get('new-password'), Auth::user()->password)) {
            //Mật khẩu hiện tại và mật khẩu mới giống nhau
            
            return redirect()->back()->with("error","Mật khẩu mới không thể giống như mật khẩu hiện tại của bạn. Vui lòng chọn một mật khẩu khác nhau.");
        }

        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|min:6|confirmed',
        ]);
        // Đổi mật khẩu
        
        $data['password'] = bcrypt($request->get('new-password'));
         
        
         DB::table('bds_member')->whereId(Auth::user()->id)->update($data);

         echo "<script>
                alert('Mật khẩu của bạn đã được thay đổi thành công.');
                window.location = '".url('/')."'
              </script>";
         
      
    }
   
    // tim kiem
    public function get_timkiem()
    {
        return view('frontend.pages.timkiem_category');
    }
   public function timkiem(Request $request) {
        $danhmuc    = $request->danhmuc;
        $province   = (int)$request->province;
        $district   = (int)$request->district;
        $dientich   = $request->dientich;
        $price      = $request->price;
        $param      = array();
        $where      = "";

        $data_dm    = DB::table('bds_dmtd')->where('parent_id', $danhmuc)->get();
        if($data_dm->count() > 0) {
            $condition  = "OR ";
            $counter    = 0;
            foreach($data_dm as $r_dm) {
                if (++$counter == count($data_dm)) {
                    $condition .= "category_id=".$r_dm->id;
                } else {
                    $condition .= "category_id=".$r_dm->id." OR ";
                }
            }
            $where  .= "WHERE category_id = $danhmuc $condition";
        }
        else {
            $where  .= "WHERE category_id = $danhmuc";
        }
       


        if($province != -1) {
            $where  .= " AND province = $province ";
            $param = array('danhmuc' => $danhmuc, 'province' => $province);
        }
        if($district != -1) {
            $where  .= " AND district = $district ";
            $param = array('danhmuc' => $danhmuc, 'province' => $province,'district' => $district);
        }

        switch ($dientich) {
            case 'less-15':
                $where  .= " AND dientich < 15 ";

                break;
            case '15-25':
                $where  .= " AND dientich >= 15 AND dientich < 25 ";

                break;
            case '25-50':
                $where  .= " AND dientich >= 25 AND dientich < 50 ";

                break;
            case '50-100':
                $where  .= " AND dientich >= 50 AND dientich < 100 ";

                break;
            case 'more-100':
                $where  .= " AND dientich > 100 ";

                break;
            
            default:
                $where  .= "";
                break;
        }

        switch ($price) {
            case 'less-1m':
                $where  .= " AND price < 1000000";

                break;
            case '1m-3m':
                $where  .= " AND price >= 1000000 AND price < 3000000";

                break;
            case '3m-5m':
                $where  .= " AND price >= 3000000 AND price < 5000000";

                break;
            case '5m-10m':
                $where  .= " AND price >= 5000000 AND price < 10000000";

                break;
            case '10m-100m':
                $where  .= " AND price >= 10000000 AND price < 100000000";

                break;
            case '100m-1b':
                $where  .= " AND price >= 100000000 AND price < 1000000000";

                break;
            case '1b-10b':
                $where  .= " AND price >= 1000000000 AND price < 10000000000";

                break;
            case 'more-10b':
                $where  .= " AND price >= 1000000000";

                break;
            
            default:
                $where  .= "";
                break;
        }
        //dd($param);
        
        $data['search_category'] = DB::select(DB::raw("SELECT * FROM bds_tindang $where"),$param);

        //dd($data);
        // Đổ data ra 1 route mới tên timkiem giống page danh mục tin đăng
       return view('frontend.pages.timkiem_category',$data); 
    }
}
