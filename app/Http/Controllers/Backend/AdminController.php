<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests\Admin\AdminStoreRequest;
use App\Http\Controllers\Controller;
use App\Http\Controllers\LoginController;
use App\Models\Admin;
use DB,DateTime,Hash,Auth;

class AdminController extends Controller
{
    private $admin;
    private $login;
    public function __construct(Admin $admin, LoginController $login){
            $this->admin = $admin;
            $this->login = $login;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function Getchangepass()
    {
        return view('admin.modules.useradmin.changepass');
    }
    public function UpdateImg($id)
    {
        $item = $this->admin->editAdmin($id);
        $filename = 'public/uploads/avatar'.$item->image;
        
        if(\File::exists($filename)){
            \File::delete($filename);
        }
    }
    public function Postchangepass(Request $request)
    {
        if (!(Hash::check($request->currentpassword, Auth::guard('admin')->user()->password))) {
            // Mật khẩu trùng khớp
            return redirect()->back()->with("error","Mật khẩu hiện tại của bạn không khớp với mật khẩu bạn cung cấp. Vui lòng thử lại.");
        }
        if($request->newpassword != $request->newpassword_confirmation) {
            //Mật khẩu hiện tại và mật khẩu mới giống nhau
            return redirect()->back()->with("error","Mật khẩu mới không thể giống như mật khẩu hiện tại của bạn. Vui lòng chọn một mật khẩu khác nhau.");
        }

        $validatedData = $request->validate([
            'currentpassword' => 'required',
            'newpassword' => 'required|min:6|confirmed',
        ]);
        // Đổi mật khẩu
        
        $data =  array('password' =>bcrypt($request->newpassword));
         
        
         DB::table('bds_admin')->whereId(Auth::guard('admin')->user()->id)->update($data);

         echo "<script>
                alert('Mật khẩu của bạn đã được thay đổi thành công.');
                window.location = '".url('/admin')."'
              </script>";
    }
    public function index()
    {
         $this->login->isAdmin();
        $data['admin'] = $this->admin->listadmin();

        return view('admin.modules.useradmin.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $this->login->isAdmin();
        return view('admin.modules.useradmin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminStoreRequest $request)
    {
        $data = array(
                'username' => $request->username,
                'fullname' => $request->fullname,
                'email'    => $request->email,
                'phone'    => $request->phone,
                'password' => bcrypt($request->password),
                'role'     => $request->role,
                'created_at' => new DateTime
        );
        if($request->hasFile('image')){
            $image = $request->file('image');
            $image_name = time(). '-'.$image->getClientOriginalName();
            $des = 'public/uploads/avatar';
            $image->move($des,$image_name);
            $data['image']=$image_name;
        } 
        
         $this->admin->add($data);
         
        return redirect()->route('useradmin.index')->with("success",'message.store_required');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->login->isAdmin();
        $data['admin'] = $this->admin->editAdmin($id);

        return view('admin.modules.useradmin.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->login->isAdmin();
        $data  = array(
                'username' => $request->username,
                'fullname' => $request->fullname,
                'email'    => $request->email,
                'phone'    => $request->phone,
                'password' => bcrypt($request->password),
                'role'     => $request->role,
                'updated_at' => new DateTime
        );
        if($request->hasFile('image')){
                $image = $request->file('image');
                $image_name = time(). '-'.$image->getClientOriginalName();
                $des = 'public/uploads/avatar';
                $image->move($des,$image_name);
                $data['image']=$image_name;

                $this->UpdateImg($id);
            } 

      

        $this->admin->capnhat($data,$id);

        return redirect()->route('useradmin.index')->with('success',"message.update_required");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {        
        $this->login->isAdmin();
        $result = $this->admin->deleteAdmin($id);
        
        if($result){
             return redirect()->back()->with('success','message.destroy_required');
         } else{
            return redirect()->back()->with('error','Bạn không có quyền xóa thành viên này');
         }    

       
    }
}
