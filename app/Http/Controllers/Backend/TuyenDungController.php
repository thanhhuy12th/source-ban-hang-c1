<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests\TuyenDung\TuyendungRequest;
use App\Http\Controllers\Controller;
use App\Models\TuyenDung;
use DateTime;

class TuyenDungController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $tuyendung;
   
    public function __construct(TuyenDung $tuyendung)
    {
        $this->tuyendung =$tuyendung;
    }
    public function updateImage($id)
    {
        $item = $this->tuyendung->edittuyendung($id);
        $img_name = 'public/uploads/postings'.$item->image;
        
        if(\File::exists($img_name)){
            \File::delete($img_name);
        }
    }
    public function index()
    {
        $data['tuyendung'] = $this->tuyendung->listtuyendung();

        return view('admin.modules.tuyendung.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.modules.tuyendung.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TuyendungRequest $request)
    {
        $data = array(
            'name'      => $request->name,
            'title'     => $request->title,
            'alias'     => (empty($request->alias))? str_slug($request->name,"-"):$request->alias,
            'intro'     => $request->intro,
            'content'   => $request->content,
            'created_at'=> new DateTime
        );
        if ($request->hasFile('image')) {
                $image = $request->file('image');
                $image_name = time(). '-'.$image->getClientOriginalName();
                $des = 'public/uploads/postings';
                $image->move($des,$image_name);
                $data['image']=$image_name;
        }
       

        $this->tuyendung->addtuyendung($data);
        
        return redirect()->route('tuyendung.index')->with("success",'message.store_required');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['tuyendung'] = $this->tuyendung->edittuyendung($id);

        return view('admin.modules.tuyendung.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array(
            'name'      => $request->name,
            'title'     => $request->title,
            'alias'     => (empty($request->alias))? str_slug($request->name,"-"):$request->alias,
            'intro'     => $request->intro,
            'content'   => $request->content,
            'created_at'=> new DateTime
        );
        if ($request->hasFile('image')) {
                $image = $request->file('image');
                $image_name = time(). '-'.$image->getClientOriginalName();
                $des = 'public/uploads/postings';
                $image->move($des,$image_name);
                $data['image']=$image_name;

                $this->updateImage($id);
        }

        $this->tuyendung->updatetuyendung($data,$id);

        return redirect()->route('tuyendung.index')->with("success",'message.update_required');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = $request->chk;
        for ($i=0; $i < count($data) ; $i++) { 
            $this->tuyendung->deltuyendung($data[$i]);
        }

        return redirect()->route('tuyendung.index')->with("success",'message.delete_required');
    }
}
