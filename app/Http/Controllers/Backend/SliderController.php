<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests\Slider\SliderRequest;
use App\Models\Slider;
use App\Http\Controllers\Controller;
use DateTime;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $slider;
    public function __construct(Slider $slider)
    { 
        $this->slider =$slider;
    }
    public function createSlider($request)
    {
        $data =array(
            'name'      => $request->name,
            'intro'     => $request->intro,
            'created_at'=> new DateTime
        );
        if($request->hasFile('image')){
                $image = $request->file('image');
                $image_name = time(). '-'.$image->getClientOriginalName();
                $des = 'public/uploads/slider';
                $image->move($des,$image_name);
                $data['image']=$image_name;
            }

        return $data; 
    }
     public function UpdateImage($id)
    {
        $item = $this->slider->editSlider($id);
        $filename = 'public/uploads/slider'.$item->image;
        
        if(\File::exists($filename)){
            \File::delete($filename);
        }
    }
    public function index()
    {
        $data['slider'] = $this->slider->listSlider();

        return view('admin.modules.slider.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.modules.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SliderRequest $request)
    {
        $data = $this->createSlider($request);

        $this->slider->addSlider($data);

        return redirect()->route('slider.index')->with("success",'message.store_required');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data['slider'] = $this->slider->editSlider($id);

        return view('admin.modules.slider.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $data =array(
            'name'      => $request->name,
            'intro'     => $request->intro,
            'created_at'=> new DateTime
        );
        if($request->hasFile('image')){
                $image = $request->file('image');
                $image_name = time(). '-'.$image->getClientOriginalName();
                $des = 'public/uploads/slider';
                $image->move($des,$image_name);
                $data['image']=$image_name;

                $this->UpdateImage($id);
            }

        $this->slider->updateSlider($data,$id);

        return redirect()->route('slider.index')->with("success",'message.update_required');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = $request->chk;

        for ($i=0; $i < count($data); $i++) { 
            $this->slider->delSlider($data[$i]);
        }

        return redirect()->route('slider.index')->with("success",'message.delete_required');
    }
}
