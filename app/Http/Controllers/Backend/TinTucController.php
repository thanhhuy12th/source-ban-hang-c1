<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests\TinTuc\NewsRequest;
use App\Http\Controllers\Controller;
use App\Models\TinTuc;
use App\Models\DMTT;
use DB,DateTime;

class TinTucController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $tintuc;
    private $danhmucTinTuc;
    public function __construct(TinTuc $tintuc, DMTT $danhmucTinTuc )
    {
        $this->news = $tintuc;
        $this->danhmucTinTuc = $danhmucTinTuc;
    }
   
   
    public function index()
    {
        $data['news'] = $this->news->listNews();

        return view('admin.modules.tintuc.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['parent'] = DB::table('bds_dmtt')->select('id','name','parent_id')->get()->toArray();
        
        $data['news_option'] = [];

        // Xử lý option danh mục tin tuc
        $news_parent = $this->danhmucTinTuc->NewsIdParent(0); // Set 0 để lây danh mục cha
        // /dd($news_parent);
        foreach ($news_parent as $key => $value) {
            // Tạo mảng 2 chiều
            $news_item = ["id"=>$value->id,"name"=>$value->name];
            // Add mảng 2 chiều vào mảng 1 chiều tổng
            array_push($data['news_option'], $news_item);

            // Lấy các danh mục con thêm vào mảng 1 chiều tổng
            $news_child = $this->danhmucTinTuc->NewsIdParent($value->id);
            
            foreach ($news_child as $keyC => $valueC) {
                // Tạo mảng 2 chiều danh mục con
                $news_child_item = ["id"=>$valueC->id,"name"=>"-- ".$valueC->name];
                array_push($data['news_option'], $news_child_item);
            }
        }
        
        return view('admin.modules.tintuc.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createNews($request)
    {
        $data  = array(            
                    'title'     => $request->title,
                    'alias'     => (empty($request->alias))? str_slug($request->title,"-"):$request->alias,                    
                    'intro'     => $request->intro,
                    'content'   => $request->content,
                    'parent_id'  => $request->parent_id,
                    'created_at'=> new DateTime
                );
        if ($request->hasFile('image')) {
                $image = $request->file('image');
                $image_name = time(). '-'.$image->getClientOriginalName();
                $des = 'public/uploads/postings';
                $image->move($des,$image_name);
                $data['image']=$image_name;
        }
        return $data;
    }
    public function store(NewsRequest $request)
    {
       $data  = array(            
                    'title'     => $request->title,
                    'alias'     => (empty($request->alias))? str_slug($request->title,"-"):$request->alias,                    
                    'intro'     => $request->intro,
                    'content'   => $request->content,
                    'parent_id'  => $request->parent_id,
                    'created_at'=> new DateTime
                );
        if ($request->hasFile('image')) 
        {
                $image = $request->file('image');
                $image_name = time(). '-'.$image->getClientOriginalName();
                $des = 'public/uploads/postings';
                $image->move($des,$image_name);
                $data['image']=$image_name;    
        }
        else
        {
            $data['image'] = "default.jpg";
           
        }

        $insert = $this->news->addNews($data);
       
        if ($insert) {
            return redirect()->route('tintuc.index')->with("success",'message.store_required');
        }
            return redirect()->back()->with("error",'Thất bại');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['news'] = $this->news->editNews($id);

         $data['news_option'] = [];

        // Xử lý option danh mục tin tuc
        $news_parent = $this->danhmucTinTuc->NewsIdParent(0); // Set 0 để lây danh mục cha
        // /dd($news_parent);
        foreach ($news_parent as $key => $value) {
            // Tạo mảng 2 chiều
            $news_item = ["id"=>$value->id,"name"=>$value->name];
            // Add mảng 2 chiều vào mảng 1 chiều tổng
            array_push($data['news_option'], $news_item);

            // Lấy các danh mục con thêm vào mảng 1 chiều tổng
            $news_child = $this->danhmucTinTuc->NewsIdParent($value->id);
            
            foreach ($news_child as $keyC => $valueC) {
                // Tạo mảng 2 chiều danh mục con
                $news_child_item = ["id"=>$valueC->id,"name"=>"-- ".$valueC->name];
                array_push($data['news_option'], $news_child_item);
            }
        }

        return view('admin.modules.tintuc.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     
    public function ImgUpdate($id)
    {
        $item = $this->news->editNews($id);
        $filename = 'public/uploads/postings'.$item->image;
        
        if(\File::exists($filename)){
            \File::delete($filename);
        }
    } 
    public function update(Request $request, $id)
    {
        $data  = array(            
                    'title'     => $request->title,
                    'alias'     => (empty($request->alias))? str_slug($request->title,"-"):$request->alias,
                    'intro'     => $request->intro,
                    'content'   => $request->content,
                    'parent_id'  => $request->parent_id,
                    'updated_at'=> new DateTime
                );
        if ($request->hasFile('image')) {
                $image = $request->file('image');
                $image_name = time(). '-'.$image->getClientOriginalName();
                $des = 'public/uploads/postings';
                $image->move($des,$image_name);
                $data['image']=$image_name;
                $this->ImgUpdate($id);
        }
        else
        {
            $data['image'] = $request->imagehidden;
           
        }

            $update = $this->news->updateNews($data,$id);
            if ($update) {
                return redirect()->route('tintuc.index')->with("success",'message.update_required');
            }
            return redirect()->back()->with("error",'Thất bại');  
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = $request->chk;
        for ($i=0; $i < count($data); $i++) { 
            $this->news->delNews($data[$i]);
        }

        return redirect()->route('tintuc.index')->with("success",'message.delete_required');
    }
}
