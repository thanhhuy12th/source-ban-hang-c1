<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests\Video\VideoRequest;
use App\Http\Controllers\Controller;
use App\Models\Video;
use DateTime;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $video;
    public function __construct(Video $video)
    {
        $this->video = $video;
    }
    public function createVideo($request)
    {
        $data = array(
            'name'          => $request->name,
            'linkyoutube'   => $request->linkyoutube,
            'status'        => $request->status,
            'created_at'    => new DateTime 
        );
        return $data;
    }
    public function index()
    {
        $data['video']  = $this->video->listVideo();

        return view('admin.modules.video.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.modules.video.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VideoRequest $request)
    {
        $data = $this->createVideo($request);

        $this->video->addVideo($data);

        return redirect()->route('video.index')->with("success",'message.store_required');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['video'] = $this->video->editVideo($id);

        return view('admin.modules.video.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $this->createVideo($request);
       
        $this->video->updateVideo($data,$id);

        return redirect()->route('video.index')->with("success",'message.update_required');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = $request->chk;
        for ($i=0; $i < count($data); $i++) { 
            $this->video->delVideo($data[$i]);
        }

        return redirect()->route('video.index')->with("success",'message.delete_required');
    }
}
