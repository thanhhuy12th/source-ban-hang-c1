<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Configuration;
use DateTime;

class ConfigurationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $configuration;
    public function __construct(Configuration  $configuration)
    {
        $this->configuration = $configuration;
    }
    public function index()
    {
        $data = [];
        $data['hotline'] = $this->configuration->editConfig(16);
        $data['email'] = $this->configuration->editConfig(17);
        $data['seo'] = $this->configuration->editConfig(18);
        $data['description'] = $this->configuration->editConfig(19);
        $data['title'] = $this->configuration->editConfig(20);
        $data['baseUrl'] = $this->configuration->editConfig(21);
        $data['address'] = $this->configuration->editConfig(22);
        $data['customerVip'] = $this->configuration->editConfig(23);
        $data['username'] = $this->configuration->editConfig(24);
        $data['password'] = $this->configuration->editConfig(25);
        $data['chatbox'] = $this->configuration->editConfig(26);
        //var_dump($data);
        //dd( $data['hotline']);
        return view('admin.modules.cauhinhkhac.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.modules.cauhinhkhac.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.modules.cauhinhkhac.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $this->configuration->updatehotline(['content'=>$request->Hotline]);     
        $this->configuration->updateEmail(['content'=>$request->Email]);
        $this->configuration->updateSEO(['content'=>$request->SEO]);
        $this->configuration->updateDescription(['content'=>$request->Description]);
        $this->configuration->updateTitle(['content'=>$request->Title]);
        $this->configuration->updateBaseUrl(['content'=>$request->BaseUrl]);
        $this->configuration->updateAddress(['content'=>$request->Address]);
        $this->configuration->updateCustomerVip(['content'=>$request->CustomerVip]);
        $this->configuration->updateUsername(['content'=>$request->Username]);
        $this->configuration->updatePassword(['content'=>$request->Password]);
        $this->configuration->updateChatbox(['content'=>$request->Chatbox]);
        //dd($request->all());
        return redirect()->route('cauhinhkhac.index')->with('success',"message.update_required");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
