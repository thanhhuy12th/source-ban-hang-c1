<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests\DMTD\DMTDRequest;
use App\Http\Controllers\Controller;
use App\Models\DMTD;
use DateTime;
use DB;

class DMTDController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $dmtd;
    public function __construct(DMTD $dmtd)
    {
        $this->dmtd = $dmtd;
    }
    public function AddCate($request)
    {
        $data = array(
                'name' =>$request->name,
                'alias' => (empty($request->alias))? str_slug($request->name,"-") : $request->alias,
                'parent_id' => $request->parent_id,
                'location'  => $request->location,
                'created_at' => new DateTime
        );

        return $data;
    }
    public function index()
    {
        $data['category'] = $this->dmtd->listcate();

        $data['opt_parent'] = $this->dmtd->getParent();

        return view('admin.modules.dmtd.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['menus'] = DB::table('bds_menu')->select('id','name')->get()->toArray();

        return view('admin.modules.dmtd.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DMTDRequest $request)
    {
        $data = $this->AddCate($request);

            $this->dmtd->addcate($data);

            return redirect()->route('dmtd.index')->with('success',"message.store_required");

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $data['menus'] = DB::table('bds_menu')->select('id','name')->get()->toArray();

        $data['category'] = $this->dmtd->showEdit($id);

        return view('admin.modules.dmtd.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $this->AddCate($request);

            $this->dmtd->updatecate($data,$id);

            return redirect()->route('dmtd.index')->with('success',"message.update_required");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        
        $data = $request->chk;
       for ($i = 0; $i < count($data); $i++){

                 $this->dmtd->delcate($data[$i]);
            }
            
       return redirect()->route('dmtd.index')->with('success',"message.delete_required"); 
    }
}
