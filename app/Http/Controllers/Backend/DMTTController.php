<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests\DMTT\DMTTRequest;
use App\Http\Controllers\Controller;
use App\Models\DMTT;
use DB,DateTime;

class DMTTController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $dmtt;
    public function __construct(DMTT $dmtt)
    {
        $this->dmtt = $dmtt;
    }
    public function createDMTT($request)
    {
        $data = array(
            'name'      => $request->name,
            'alias'     => (empty($request->alias))? str_slug($request->name,"-") : $request->alias,
            'parent_id' => $request->parent_id,
            'created_at'=> new DateTime
        );
        return $data;
    }
    public function index()
    {
        $data['cate']  = $this->dmtt->listindex();

        return view('admin.modules.dmtt.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $data['menus'] = DB::table('bds_dmtt')->select('id','name','parent_id')->get()->toArray();

        return view('admin.modules.dmtt.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DMTTRequest $request)
    {
        $data = $this->createDMTT($request);    

        $this->dmtt->adddmtt($data);

       return redirect()->route('dmtt.index')->with('success',"message.store_required");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         // $data['menus'] = DB::table('bds_dmtt')->select('id','name','parent_id')->get()->toArray();

        $data['cate'] = $this->dmtt->showEdit($id);

        return view('admin.modules.dmtt.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $data = $this->createDMTT($request);        

         $this->dmtt->updatedmtt($data,$id);
         
         return redirect()->route('dmtt.index')->with('success',"message.update_required");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = $request->chk;
       for ($i = 0; $i < count($data); $i++){

                 $this->dmtt->delnews($data[$i]);
            }
            
       return redirect()->route('dmtt.index')->with('success',"message.delete_required"); 
    }
    
}
