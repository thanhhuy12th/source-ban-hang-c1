<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {
    	$data = [];

    	$data['role'] = (Auth::guard('admin')->user()->role);
    	$data['fullname'] =(Auth::guard('admin')->user()->fullname);
    	
    	return view('admin.dashboard.index',$data);
    }
}
