<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use DB;

class HeaderComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
     public function compose(View $view)
    {
        $count = DB::table('bds_admin')->count();
        $view->with('count', $count);

        $logo = DB::table('bds_config')->whereId(5)->first();
        $view->with('logo',$logo);
    }
}