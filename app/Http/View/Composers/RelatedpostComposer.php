<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use DB;

class RelatedpostComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
     public function compose(View $view)
    {
        // thông tin dự án
        $duan = DB::table('bds_duan')->where('status',1)->orderBy('id', 'DESC')->limit(10)->paginate(6);
        $view->with('duan', $duan);

        // bài viết liên quan
        $product_category = DB::table('bds_tindang')->where('status',1)->orderBy('id', 'DESC')->limit(10)->paginate(6);
        $view->with('product_category', $product_category);

        // tin mới nhất
        $newsnews = DB::table('bds_tintuc')->select('title','alias')->orderBy('id','DESC')->limit(10)->paginate(6);
         $view->with('newsnews', $newsnews);
    }

}