<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use DB;

class DashboardComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
     public function compose(View $view)
    {
        $project = DB::table('bds_duan')->where('status',1)->orderBy('id', 'DESC')->limit(5)->get()->toArray();
        $view->with('project', $project);

        $news = DB::table('bds_tintuc')->orderBy('id', 'DESC')->limit(5)->get()->toArray();
        $view->with('news', $news);

        $member = DB::table('bds_member')->orderBy('id', 'DESC')->limit(5)->get()->toArray();
        $view->with('member', $member);

        $postings = DB::table('bds_tindang')->where('status',1)->orderBy('id', 'DESC')->limit(5)->get()->toArray();
        $view->with('postings', $postings);

        $count_member = DB::table('bds_member')->count();
        $view->with('count_member', $count_member);

        $count_duan = DB::table('bds_duan')->count();
        $view->with('count_duan', $count_duan);

        $count_tindang = DB::table('bds_tindang')->count();
        $view->with('count_tindang', $count_tindang);

    }
}