<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;
use DB,Session;

class MenuComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    public function compose(View $view)
    {
        $menu = DB::table('bds_menu')->where('parent_id',0)->get()->toArray();
        $view->with('menu',$menu);

        $cates_2 = DB::table('bds_dmtd')->where('parent_id',2)->get();
        $view->with('cates_2', $cates_2);

       $cates_3 = DB::table('bds_dmtd')->where('parent_id',3)->get();
       $view->with('cates_3', $cates_3);

       $hotline1 = DB::table('bds_config')->whereId(9)->first();
       $view->with('hotline1',$hotline1);

       $phonename1 = DB::table('bds_config')->whereId(9)->first();
       $view->with('phonename1',$phonename1);

       $hotline2 = DB::table('bds_config')->whereId(10)->first();
       $view->with('hotline2',$hotline2);

       $phonename2 = DB::table('bds_config')->whereId(10)->first();
       $view->with('phonename2',$phonename2);

       $hotline3 = DB::table('bds_config')->whereId(11)->first();
       $view->with('hotline3',$hotline3);

       $phonename3 = DB::table('bds_config')->whereId(11)->first();
       $view->with('phonename3',$phonename3);

       // $logo = DB::table('bds_config')->whereId(2)->first();
       // $view->with('logo',$logo);

       // $member = DB::table('bds_member')->select('image')->get()->toArray();
       // $view->with('member',$member);
       
       if (Auth::check()) {

           $view->with('user', Auth::user());
       }


      $menu_lv_1 = DB::table('bds_dmtd')->where('parent_id', 0)->get();
       $view->with('menu_lv_1',$menu_lv_1);
                        
    }
}