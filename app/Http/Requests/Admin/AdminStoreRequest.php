<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AdminStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'  => 'required|unique:bds_admin,username',
            'fullname'  => 'required|unique:bds_admin,fullname',
            'email'     => 'required|unique:bds_admin,email',
            'phone'     => 'required|min:11|numeric',
            'password'  => 'required|min:6|confirmed',
            'role'      => 'required',
           // 'image'     => 'required|mimes:jpeg,jpg,png,gif,tiff,bmp,svg|max:5000',

        ];
    }
    public function messages()
    {
        return [
            'username.required'         => trans('message.username_required'),
            'username.unique'           => trans('message.username_unique'),
            'fullname.required'         => trans('message.fullname_required'),
            'fullname.unique'           => trans('message.fullname_unique'),
            'email.required'            => trans('message.email_required'),
            'email.unique'              => trans('message.email_unique'),
            'phone.required'            => trans('message.phone_required'),
            'phone.min'                 => trans('message.phone_min'),
            'phone.numeric'             => trans('message.phone_numeric'),
            'password.required'         => trans('message.password_required'),
            'password.min'              => trans('message.password_min'),
            'password.confirmed'        => trans('message.password_confirmed'),
            'role.required'             => trans('message.role_required'),
            /*'image.required'            => trans('message.image_required'),
            'image.mimes'               => trans('message.image_mimes'),
            'image.max'                 => trans('message.image_max'),*/
        ];
    }

}
