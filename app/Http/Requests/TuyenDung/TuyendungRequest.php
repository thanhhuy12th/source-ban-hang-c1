<?php

namespace App\Http\Requests\TuyenDung;

use Illuminate\Foundation\Http\FormRequest;

class TuyendungRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required|unique:bds_tuyendung,name',
            'title'     => 'required|unique:bds_tuyendung,title',
            'intro'     => 'required',
            'content'   => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required'         => trans('message.name_required'),
            'name.unique'           => trans('message.name_unique'),
            'title.required'        => trans('message.title_required'),
            'title.unique'          => trans('message.title_unique'),
            'intro.required'        => trans('message.intro_required'),
            'content.required'      => trans('message.content_required'),
        ];
    }
}
