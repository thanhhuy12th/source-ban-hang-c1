<?php

namespace App\Http\Requests\TinTuc;

use Illuminate\Foundation\Http\FormRequest;

class NewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'     => 'required|unique:bds_tintuc,title|max:150',
            'intro'     => 'required',
            'content'   => 'required',
            'image'     => 'required|mimes:jpeg,jpg,png,gif,tiff,bmp,svg|max:5000'
        ];
    }
    public function messages()
    {
        return [
            'title.required'        => trans('message.title_required'),
            'title.unique'          => trans('message.title_unique'),
            'title.max'             => trans('message.title_max'),
            'intro.required'        => trans('message.intro_required'),
            'content.required'      => trans('message.content_required'),
            'image.required'        => trans('message.image_required'),
            'image.mimes'           => trans('message.image_mimes'),
            'image.max'             => trans('message.image_max'),

        ];
    }
}
