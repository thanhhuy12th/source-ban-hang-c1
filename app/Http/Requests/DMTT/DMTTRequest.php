<?php

namespace App\Http\Requests\DMTT;

use Illuminate\Foundation\Http\FormRequest;

class DMTTRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required|unique:bds_dmtd,name',
            'parent_id' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required'         => trans('message.name_required'),
            'name.unique'           => trans('message.name_unique'),
            'parent_id.required'    => trans('message.parent_id_required'),
        ];
    }
}
