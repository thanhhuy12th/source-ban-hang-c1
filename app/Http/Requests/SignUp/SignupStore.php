<?php

namespace App\Http\Requests\SignUp;

use Illuminate\Foundation\Http\FormRequest;

class SignupStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'  => 'required|mix:6|max:25|unique:bds_member,username',
            'fullname'  => 'required||mix:10|max:50|unique:bds_member,fullname',
            'email'     => 'required|unique:bds_member,email',
            'phone'     => 'required|min:11|numeric',
            'address'   => 'required',
            'password'  => 'required|min:6|max:25|confirmed',
        ];
    }
    public function messages()
    {
        return [
            'username.required'         => trans('message.username_required'),
            'username.min'              => trans('message.username_min'),
            'username.max'              => trans('message.username_max'),
            'username.unique'           => trans('message.username_unique'),
            'fullname.required'         => trans('message.fullname_required'),
            'fullname.min'              => trans('messages.fullname_min'),
            'fullname.max'              => trans('messages.fullname_max'),
            'fullname.unique'           => trans('message.fullname_unique'),
            'email.required'            => trans('message.email_required'),
            'email.unique'              => trans('message.email_unique'),
            'phone.required'            => trans('message.phone_required'),
            'phone.min'                 => trans('message.phone_min'),
            'phone.numeric'             => trans('message.phone_numeric'),
            'address.required'          => trans('message.address_required'),
            'password.required'         => trans('message.password_required'),
            'password.min'              => trans('message.password_min'),
            'password.max'              => trans('message.password_max'),
            'password.confirmed'        => trans('message.password_confirmed'),
        ];
    }
}
