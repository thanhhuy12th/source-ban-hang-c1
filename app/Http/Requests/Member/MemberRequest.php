<?php

namespace App\Http\Requests\Member;

use Illuminate\Foundation\Http\FormRequest;

class MemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fullname'   => 'required',
            'username'   => 'required',
            'email'      => 'required|max:25',
            'phone'      => 'required|min:11|numeric',
            'address'    => 'required|max:120',
           
        ];
    }
    public function messages()
    {
        return [
            'fullname.required'         => trans('message.fullname_required'),
            'username.required'         => trans('message.username_required'),
            'email.required'            => trans('message.email_required'),
            'email.max'                 => trans('message.email_max'),
            'phone.required'            => trans('message.phone_required'),
            'phone.min'                 => trans('message.phone_min'),
            'phone.numeric'             => trans('message.phone_numeric'),
            'address.required'          => trans('message.address_required'),
            'address.max'               => trans('message.address_max'),
        ];
    }
}
