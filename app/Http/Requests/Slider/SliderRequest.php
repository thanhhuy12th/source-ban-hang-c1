<?php

namespace App\Http\Requests\Slider;

use Illuminate\Foundation\Http\FormRequest;

class SliderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required|unique:bds_slider,name',
            'intro'     => 'required',
            'image'     => 'required|mimes:jpeg,jpg,png,gif,tiff,bmp,svg|max:5000',
        ];
    }

    public function messages()
    {
        return [
            'name.required'             => trans('message.name_required'),
            'name.unique'               => trans('message.name_unique'),
            'intro.required'            => trans('message.intro_required'),
            'image.required'            => trans('message.image_required'),
            'image.mimes'               => trans('message.image_mimes'),
            'image.max'                 => trans('message.image_max'),
        ];
    }
}
