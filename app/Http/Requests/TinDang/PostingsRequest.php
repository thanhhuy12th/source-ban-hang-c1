<?php

namespace App\Http\Requests\TinDang;

use Illuminate\Foundation\Http\FormRequest;

class PostingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'title'     => 'required',
            'dientich'  => 'required|numeric:bds_tindang,dientich',
            'price'     => 'required|numeric:bds_tindang,dientich',
            'vitri'     => 'required',
            'intro'     => 'required',
            // 'hinh'      => 'max:2000'
            //'status'    => 'required',
        ];
    }
    public function messages()
    {
        return [
           
            'title.required'        => trans('message.title_required'),
            'dientich.required'     => trans('message.dientich_required'),
            'dientich.numeric'      => trans('message.dientich_numeric'),
            'price.required'        => trans('message.price_required'),
            'price.numeric'         => trans('message.price_numeric'),
            'vitri.required'        => trans('message.vitri_required'),
            //'status.required'       => trans('message.status_required'),
            'intro.required'        => trans('message.intro_required'),
            // 'hinh.max'              => trans('message.hinh_max'),
        ];
    }
}
