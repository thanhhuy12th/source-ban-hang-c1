<?php

namespace App\Http\Requests\Video;

use Illuminate\Foundation\Http\FormRequest;

class VideoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'       => 'required|unique:bds_video,name',
            'linkyoutube'=> 'required',
            'status'     => 'required'
        ];
    }
    public function messages()
    {
        return [
            'name.required'         => trans('message.name_required'),
            'name.unique'           => trans('message.name_unique'),
            'linkyoutube.required'  => trans('message.linkyoutube_required'),
            'status.required'       => trans('message.status_required'),
        ];
    }
}
