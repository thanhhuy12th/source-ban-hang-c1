<?php

namespace App\Http\Requests\LienHe;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'name'       => 'required|unique:bds_lienhe,name',
           'email'      => 'required|unique:bds_lienhe,email',
           'phone'      => 'required|min:11|numeric',
           'address'    => 'required',
           'content'    => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required'         => trans('message.name_required'),
            'name.unique'           => trans('message.name_unique'),
            'email.required'        => trans('message.email_required'),
            'email.unique'          => trans('message.email_unique'),
            'phone.required'            => trans('message.phone_required'),
            'phone.min'                 => trans('message.phone_min'),
            'phone.numeric'             => trans('message.phone_numeric'),
            'address.required'      => trans('message.address_required'),
            'content.required'      => trans('message.content_required'),
        ];
    }
}
