<?php

namespace App\Http\Requests\DMTD;

use Illuminate\Foundation\Http\FormRequest;

class DMTDRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required', 
            
        ];
    }
    public function messages()
    {
        return
        [
            'name.required'         => trans('message.name_required'),
           
        ];
        
    }
}
